use db::DbInstance;
// use std::sync::Arc;
// use serenity::prelude::Context;
// use serde::{Serialize, Deserialize};
use warp::Filter;
// use oauth2::{
//     ClientId,
//     ClientSecret,
//     AuthUrl,
//     TokenUrl,
//     Scope,
//     RedirectUrl,
//     basic::BasicClient,
//     reqwest::http_client,
// };

// #[derive(Debug, Serialize, Deserialize)]
// struct Claim {
//     user_id: u64,
// }

// struct AppState {
//     discord_oauth: BasicClient,
// }

pub async fn start(ip: std::net::SocketAddr, _db: DbInstance) {
    // let discord_oauth = BasicClient::new(
    //     ClientId::new(client_id),
    //     Some(ClientSecret::new(client_secret)),
    //     AuthUrl::new("https://discord.com/api/oauth2/authorize".to_string()).unwrap(),
    //     Some(TokenUrl::new("https://discord.com/api/oauth2/token".to_string()).unwrap()),
    // )
    // .set_redirect_uri(RedirectUrl::new("".to_string()).unwrap())
    // .set_revocation_uri(RevocationUrl::new("https://discord.com/api/oauth2/token/revoke"));
//
    // let state = Arc::new(AppState {
    //     discord_oauth
    // });

    let index = warp::get()
        .and(warp::fs::file("./web/index.html"));

    let wasm_assets = warp::path("wasm")
        .and(warp::fs::dir("./web/dist"));

    let static_assets = warp::path("static")
        .and(warp::fs::dir("./web/static"));

    let routes = wasm_assets
        .or(static_assets)
        .or(index);

    warp::serve(routes).run(ip).await;
}
