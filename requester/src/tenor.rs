use crate::{Requester, Result};
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Clone)]
pub struct TenorSearchOption<'a> {
    pub key: &'a str,
    pub query: &'a str,
    pub contentfilter: ContentFilter,
    pub media_filter: Option<MediaFilter>,
    pub ar_range: ArRange,
    pub limit: u8,
    pub pos: Option<&'a str>,
}

impl Default for TenorSearchOption<'_> {
    fn default() -> Self {
        Self {
            key: "",
            query: "",
            contentfilter: ContentFilter::Off,
            media_filter: None,
            ar_range: ArRange::All,
            limit: 20,
            pos: None,
        }
    }
}

impl TenorSearchOption<'_> {
    fn append_query(&self, url: &mut Url) {
        let mut query_pairs = url.query_pairs_mut();

        query_pairs
            .clear()
            .append_pair("key", self.key)
            .append_pair("q", self.query)
            .append_pair("limit", &self.limit.to_string())
            .append_pair(
                "contentfilter",
                match self.contentfilter {
                    ContentFilter::Off => "off",
                    ContentFilter::Low => "low",
                    ContentFilter::Medium => "medium",
                    ContentFilter::High => "high",
                },
            )
            .append_pair(
                "ar_range",
                match self.ar_range {
                    ArRange::All => "all",
                    ArRange::Wide => "wide",
                    ArRange::Standard => "standard",
                },
            );

        if let Some(filter) = self.media_filter {
            query_pairs.append_pair(
                "media_filter",
                match filter {
                    MediaFilter::Basic => "basic",
                    MediaFilter::Minimal => "minimal",
                },
            );
        }

        if let Some(pos) = self.pos {
            query_pairs.append_pair("pos", pos);
        }
    }
}

#[derive(Clone, Copy)]
pub enum ContentFilter {
    Off,
    Low,
    Medium,
    High,
}

#[derive(Clone, Copy)]
pub enum MediaFilter {
    Basic,
    Minimal,
}

#[derive(Clone, Copy)]
pub enum ArRange {
    All,
    Wide,
    Standard,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TenorSearch {
    pub next: String,
    pub results: Vec<TenorImage>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TenorImage {
    pub created: f64,
    pub hasaudio: bool,
    pub id: String,
    pub title: String,
    pub itemurl: String,
    pub hascaption: bool,
    pub url: String,
    pub tags: Vec<String>,
    pub media: Vec<TenorMedia>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TenorMedia {
    pub gif: TenorMediaObject,
    pub mediumgif: TenorMediaObject,
    pub tinygif: TenorMediaObject,
    pub nanogif: TenorMediaObject,
    pub mp4: TenorMediaObject,
    pub loopedmp4: TenorMediaObject,
    pub tinymp4: TenorMediaObject,
    pub nanomp4: TenorMediaObject,
    pub webm: TenorMediaObject,
    pub tinywebm: TenorMediaObject,
    pub nanowebm: TenorMediaObject,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TenorMediaObject {
    pub size: usize,
    pub url: String,
    pub preview: String,
    pub dims: (u32, u32),
}

#[async_trait::async_trait]
pub trait TenorApi: Requester {
    async fn search(&self, opt: TenorSearchOption<'_>) -> Result<TenorSearch> {
        let mut url = Url::parse("https://g.tenor.com/v1/search")?;
        opt.append_query(&mut url);
        self.get(url.as_str()).await
    }

    async fn random(&self, opt: TenorSearchOption<'_>) -> Result<TenorSearch> {
        let mut url = Url::parse("https://g.tenor.com/v1/random")?;
        opt.append_query(&mut url);
        self.get(url.as_str()).await
    }
}

impl<R: Requester + ?Sized> TenorApi for R {}
