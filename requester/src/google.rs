use crate::{Result, Scraper};
use scraper::Selector;
use url::Url;
// use std::pin::Pin;
// use std::task::{Context as FutContext, Poll};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct GoogleSearchData {
    pub title: String,
    pub description: String,
    pub link: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct GoogleImageData {
    pub title: String,
    pub url: String,
    pub img_url: String,
    pub width: usize,
    pub height: usize,
}

#[derive(Debug, Clone, Copy)]
pub enum Filter {
    Nsfw,
    Sfw,
}

impl From<bool> for Filter {
    fn from(b: bool) -> Self {
        if b {
            Self::Sfw
        } else {
            Self::Nsfw
        }
    }
}

// pub struct GoogleSearchStream<'a> {
//     data: Vec<GoogleSearchData>,
//     req: &'a dyn Scraper,
//     keyword: &'a str,
//     page: usize,
// }
//
// impl<'a> Stream for GoogleSearchStream<'a> {
//     type Item = GoogleSearchData;
//     fn poll_next(mut self: Pin<&mut Self>, ctx: &mut FutContext<'_>) -> Poll<Option<Self::Item>> {
//
//     }
// }

#[async_trait]
pub trait GoogleScraper: Scraper {
    async fn google_search(&self, text: &str) -> Result<Vec<GoogleSearchData>> {
        let url = Url::parse_with_params(
            "https://www.google.com/search",
            &[("hl", "en"), ("q", text)],
        )?;

        let html = self.html(url.as_ref()).await?;

        let item_selector = Selector::parse(".g > .rc").unwrap();
        let title_selector = Selector::parse(".LC20lb.DKV0Md").unwrap();
        let description_selector = Selector::parse(".s .st").unwrap();
        let link_selector = Selector::parse(".r > a").unwrap();

        let mut result = Vec::new();

        for element in html.select(&item_selector) {
            let data = (|| {
                let title = element
                    .select(&title_selector)
                    .next()
                    .map(|e| e.text().collect::<String>())
                    .filter(|e| !e.is_empty())?;

                let description = element
                    .select(&description_selector)
                    .next()?
                    .text()
                    .collect::<String>();

                let link = element
                    .select(&link_selector)
                    .next()?
                    .value()
                    .attr("href")?
                    .to_owned();

                Some(GoogleSearchData {
                    title,
                    description,
                    link,
                })
            })();

            if let Some(value) = data {
                result.push(value);
            }
        }

        Ok(result)
    }

    async fn google_image<F: Into<Filter> + Send + Sync>(
        &self,
        text: &str,
        filter: F,
    ) -> Result<Vec<GoogleImageData>> {
        let filter = match filter.into() {
            Filter::Nsfw => "images",
            Filter::Sfw => "active",
        };

        let mut url = Url::parse("https://www.google.com/search")?;

        url.query_pairs_mut()
            .append_pair("q", text)
            .append_pair("hl", "en")
            .append_pair("safe", &filter.to_string())
            .append_pair("tbm", "isch");

        let text = self.text(url.as_ref()).await?;

        let mut result = Vec::new();
        let mut lines = text.lines().map(|v| v.trim_start());

        while let Some(line) = lines.next() {
            if !(line.contains("http") && line.matches(',').count() == 3) {
                continue;
            }

            let data = (|| {
                let start = line.find('"')?;
                let end = line.rfind('"')?;
                let img_url = line.get(start + 1..end)?.to_owned();

                let mut iter = line[end..line.len() - 1].split(',');
                let width = iter.nth(1)?.parse::<usize>().ok()?;
                let height = iter.next()?.parse::<usize>().ok()?;

                let mut iter = lines.nth(2)?.split(',');
                let url = iter.nth(3)?.trim_matches('"').to_owned();
                let title = iter.next()?.trim_matches('"').to_owned();

                Some(GoogleImageData {
                    img_url,
                    width,
                    height,
                    url,
                    title,
                })
            })();

            if let Some(value) = data {
                result.push(value);
            }
        }

        Ok(result)
    }
}

impl<S: Scraper + ?Sized> GoogleScraper for S {}
