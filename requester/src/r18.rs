use crate::Result;
use async_trait::async_trait;
#[cfg(feature = "serenity-ext")]
use core::num::NonZeroUsize;
use magic::traits::MagicIter as _;
use scraper::Selector;
#[cfg(feature = "serenity-ext")]
use serenity_ext::*;
use url::Url;

#[cfg(feature = "scrapers")]
#[derive(Default, Debug, Serialize, Deserialize)]
pub struct SearchData {
    pub contents: Vec<SearchContent>,
}

#[cfg(feature = "scrapers")]
#[derive(Default, Debug, Serialize, Deserialize)]
pub struct SearchContent {
    pub image_url: String,
    pub id: String,
    pub label: String,
    pub title: String,
    pub actress: Option<String>,
    pub price: String,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct R18Content {
    pub status: String,
    pub data: Option<R18ContentData>,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct R18ContentData {
    pub content_id: String,
    pub dvd_id: String,
    pub title: String,
    pub comment: Option<String>,
    pub runtime_minutes: String,
    pub release_date: String,
    pub maker: R18ContentDataInfo,
    pub label: R18ContentDataInfo,
    pub series: Option<R18ContentDataInfo>,
    pub categories: Vec<R18ContentDataInfo>,
    pub actresses: Vec<R18ContentDataInfo>,
    pub channels: Vec<R18ContentDataInfo>,
    pub sample: R18ContentDataSample,
    pub images: R18ContentDataImage,
    pub detail_url: String,
    // pub is_mobile: String,
    pub gallery: Vec<R18Image>,
    pub director: String,
    pub languages: String,
    pub is_vr: bool,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct R18ContentDataInfo {
    pub id: String,
    pub name: String,
}

macro_rules! impl_id_url {
    ( $( $x:tt $y:tt ),* ) => {
        impl R18ContentDataInfo {
            $(
                pub fn $x(&self) -> String {
                    format!($y, self.id)
                }
            )*
        }
    }
}

impl_id_url! {
    maker_url "https://www.r18.com/videos/vod/movies/list/id={}/pagesize=30/price=all/sort=popular/type=studio/page=1/",
    series_url "https://www.r18.com/videos/vod/movies/list/id={}/pagesize=30/price=all/sort=popular/type=category/page=1/",
    category_url "https://www.r18.com/videos/vod/movies/list/id={}/pagesize=30/price=all/sort=popular/type=category/page=1/",
    actress_url "https://www.r18.com/videos/vod/movies/list/id={}/pagesize=30/price=all/sort=popular/type=actress/page=1/"
}

impl R18ContentDataInfo {
    pub fn actress_image_url(&self) -> String {
        format!(
            "https://pics.r18.com/mono/actjpgs/{}.jpg",
            self.name.to_lowercase().split_whitespace().join("_")
        )
    }

    pub fn channel_url(&self) -> String {
        format!(
            "https://www.r18.com/videos/channels/{}/pagesize=30/sort=ch_new/type=all/page=1/",
            self.name.to_lowercase().split_whitespace().join("_")
        )
    }
}

// #[derive(Default, Debug, Serialize, Deserialize)]
// pub struct R18ContentDataActress {
//     pub id: String,
//     pub name: String,
//     pub image_url: String,
//     pub actress_url: String,
// }

// #[derive(Default, Debug, Serialize, Deserialize)]
// pub struct R18ContentDataChannel {
//     pub id: String,
//     pub name: String,
//     pub channel_url: String,
// }

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct R18ContentDataSample {
    pub low: String,
    pub medium: String,
    pub high: String,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct R18ContentDataImage {
    pub jacket_image: R18Image,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct R18Image {
    pub large: String,
    pub medium: String,
    pub small: String,
}

#[async_trait]
pub trait R18Api: crate::Requester {
    async fn r18content(&self, id: &str) -> Result<R18Content> {
        let mut url = Url::parse("https://www.r18.com/api/v4f/contents")?;
        url.path_segments_mut().unwrap().push(id);
        url.set_query(Some("lang=en&unit=USD"));
        self.get(dbg!(url.as_ref())).await
    }
}

#[cfg(feature = "scrapers")]
#[async_trait]
pub trait R18Scraper: crate::Scraper {
    async fn r18search(&self, query: &str, page: u32) -> Result<SearchData> {
        let mut url = Url::parse("https://www.r18.com/common/search")?;
        url.path_segments_mut()
            .unwrap()
            .push(&format!("searchword={}", query))
            .push(&format!("page={}", page));

        let html = self.html(url.as_ref()).await?;
        let item_selector = Selector::parse("li.item-list").unwrap();
        let img_selector = Selector::parse("img").unwrap();
        let data_selector = Selector::parse("a.js-view-sample").unwrap();

        let mut contents = Vec::new();

        for item in html.select(&item_selector) {
            let data = (|| {
                let img = item.select(&img_selector).next()?.value();
                let label = img.attr("alt")?.to_owned();
                let image_url = img.attr("data-original")?.to_owned();

                let data = item.select(&data_selector).next()?.value();
                let id = data.attr("data-id")?.to_owned();
                let price = data.attr("data-price")?.to_owned();
                let title = data.attr("data-title")?.to_owned();
                let actress = data.attr("data-actress").map(String::from);

                Some(SearchContent {
                    image_url,
                    title,
                    actress,
                    id,
                    label,
                    price,
                })
            })();

            if let Some(data) = data {
                contents.push(data);
            }
        }

        Ok(SearchData { contents })
    }
}

impl<R: crate::Requester + ?Sized> R18Api for R {}
#[cfg(feature = "scrapers")]
impl<R: crate::Scraper + ?Sized> R18Scraper for R {}

#[cfg(feature = "serenity-ext")]
impl Embedable for R18Content {
    fn append(&self, embed: &mut CreateEmbed) {
        let content = match self.data {
            Some(ref data) => data,
            None => {
                embed.description(&self.status);
                return;
            }
        };

        embed.title(&content.title);
        embed.url(&content.detail_url);
        embed.image(&content.images.jacket_image.large);
        embed.field("ID", &content.dvd_id, true);
        embed.field(
            "Duration",
            &format!("{} minutes", content.runtime_minutes),
            true,
        );
        embed.field("Release Date", &content.release_date, true);
        embed.field("Maker", &content.maker.name, true);
        embed.field("Director", &content.director, true);

        if let Some(ref series) = content.series {
            embed.field("Series", &series.name, true);
        }

        let channels = content.channels.iter().map(|v| &v.name).join(", ");
        embed.field("Channels", channels, true);

        let actresses = content.actresses.iter().map(|v| &v.name).join(", ");
        embed.field("Actresses", actresses, false);

        const NONSENSE_CATEGORIES: &[&str] = &["Hi-Def", "Featured Actress"];
        let categories = content
            .categories
            .iter()
            .map(|v| &v.name)
            .filter(|v| NONSENSE_CATEGORIES.iter().all(|x| v != x))
            .join(", ");

        embed.field("Categories", categories, false);

        let find_links = format!(
            "[Sukebei](https://sukebei.nyaa.si/?f=0&c=0_0&q={})\n[Daftsex](https://daftsex.com/video/{})",
            &content.dvd_id,
            &content.dvd_id,
        );

        embed.field("Find it on", find_links, false);
    }
}

#[cfg(all(feature = "serenity-ext", feature = "scrapers"))]
impl Paginator for SearchData {
    fn total_pages(&self) -> Option<usize> {
        Some(self.contents.len())
    }

    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed) {
        let idx = page.get() - 1;
        let content = match self.contents.get(idx) {
            Some(data) => data,
            None => {
                embed.description("Failed to get data");
                return;
            }
        };

        embed.title(&content.title);
        embed.thumbnail(&content.image_url);
        embed.field("Label", &content.label, true);

        if let Some(actress) = &content.actress {
            embed.field("Actress", actress, true);
        }

        embed.field("Price", &content.price, true);
    }
}
