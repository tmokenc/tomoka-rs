use crate::Result;
use std::collections::HashMap;

const API_END_POINT: &str = "http://mazii.net/api/search";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MaziiKanjiSearch {
    pub status: u16,
    pub results: Vec<MaziiKanji>,
}

// Type Kanji
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MaziiKanji {
    pub kanji: char,
    pub mean: String,
    pub on: String,
    pub kun: Option<String>,
    pub detail: Option<String>,
    pub comp: Option<String>,
    pub level: Option<char>,
    pub stoke_count: Option<char>,
    pub example_on: Option<HashMap<String, Vec<Example>>>,
    pub example_kun: Option<HashMap<String, Vec<Example>>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Example {
    #[serde(alias = "w")]
    pub word: String,
    #[serde(alias = "p")]
    pub phonetic: String,
    #[serde(alias = "m")]
    pub meaning: String,
}

impl MaziiKanji {
    pub fn normal_detail(&self) -> Option<String> {
        self.detail.as_ref().map(|e| {
            e.split("##")
                .map(|v| format!("- {}\n", v))
                .collect::<String>()
        })
    }

    pub fn normal_on(&self) -> String {
        self.on.split(' ').collect::<Vec<&str>>().join("、")
    }

    pub fn normal_kun(&self) -> Option<String> {
        self.kun
            .as_ref()
            .map(|e| e.split(' ').collect::<Vec<_>>().join("、"))
    }
}

#[derive(Serialize)]
struct MaziKanjiBody<'a> {
    dict: &'a str,
    r#type: &'a str,
    query: &'a str,
    page: u16,
}

#[async_trait]
pub trait MaziiApi: crate::Requester {
    async fn kanji(&self, kanji: &str) -> Result<MaziiKanjiSearch> {
        let body = MaziKanjiBody {
            dict: "javi",
            r#type: "kanji",
            query: kanji,
            page: 1,
        };

        let mut data: MaziiKanjiSearch = self.post(API_END_POINT, &body).await?;
        data.results
            .sort_by_key(|a| kanji.chars().position(|x| x == a.kanji));

        Ok(data)
    }
}

impl<R: crate::Requester + ?Sized> MaziiApi for R {}

#[cfg(feature = "serenity-ext")]
mod serenity_impl {
    use magic::traits::MagicStr as _;
    use serenity_ext::*;

    impl Embedable for super::MaziiKanjiSearch {
        fn append(&self, embed: &mut CreateEmbed) {
            embed.color(0x977df2);

            if self.results.is_empty() {
                embed.description("Cannot find any kanji...");
                return;
            }

            for kanji in &self.results {
                let info = format!(
                    "{} - {level} {mean} | {on} {kun}",
                    kanji.kanji,
                    mean = kanji.mean,
                    on = kanji.normal_on(),
                    level = kanji.level.map(|l| format!("(N{})", l)).unwrap_or_default(),
                    kun = kanji
                        .normal_kun()
                        .map(|k| format!("| {}", k))
                        .unwrap_or_default()
                );

                let detail = kanji
                    .normal_detail()
                    .and_then(|d| d.split_at_limit(1024, "\n").next().map(String::from))
                    .unwrap_or_default();

                embed.field(info, detail, false);
            }
        }
    }

    impl Embedable for super::MaziiKanji {
        fn append(&self, embed: &mut CreateEmbed) {
            let info = format!(
                "{} - {level} {mean} | {on} {kun}",
                self.kanji,
                mean = self.mean,
                on = self.normal_on(),
                level = self.level.map(|l| format!("(N{})", l)).unwrap_or_default(),
                kun = self
                    .normal_kun()
                    .map(|k| format!("| {}", k))
                    .unwrap_or_default()
            );

            let detail = self
                .normal_detail()
                .and_then(|d| d.split_at_limit(1024, "\n").next().map(String::from))
                .unwrap_or_default();

            embed.field(info, detail, false);
        }
    }
}
