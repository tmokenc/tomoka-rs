use crate::{Result, Scraper};
use scraper::{ElementRef, Html, Selector};
use serde::de::{self, Visitor};
use serde::Deserializer;
use std::fmt;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct NhentaiGallery {
    #[serde(deserialize_with = "string_or_u64")]
    pub id: u64,
    #[serde(deserialize_with = "string_or_u64")]
    pub media_id: u64,
    pub title: Title,
    pub images: Images,
    pub scanlator: String,
    pub upload_date: u64,
    pub tags: Vec<Tag>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Title {
    pub english: String,
    pub japanese: Option<String>,
    pub pretty: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Images {
    pub pages: Vec<Page>,
    pub cover: Page,
    pub thumbnail: Page,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Page {
    #[serde(rename = "t")]
    pub ext: PageType,
    pub w: u16,
    pub h: u16,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum PageType {
    #[serde(rename = "j")]
    Jpg,
    #[serde(rename = "p")]
    Png,
    #[serde(rename = "g")]
    Gif,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Tag {
    pub id: u64,
    pub r#type: NhentaiType,
    pub name: String,
    pub url: String,
    pub count: u64,
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum NhentaiType {
    Category,
    Language,
    Tag,
    Group,
    Parody,
    Artist,
    Character,
}

impl fmt::Display for PageType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::Jpg => write!(f, "jpg"),
            Self::Png => write!(f, "png"),
            Self::Gif => write!(f, "gif"),
        }
    }
}

#[cfg(feature = "extra")]
#[derive(Debug, Clone, Default)]
pub struct NhentaiMetadata<'a> {
    pub categories: Vec<&'a str>,
    pub languages: Vec<&'a str>,
    pub parodies: Option<Vec<&'a str>>,
    pub tags: Option<Vec<&'a str>>,
    pub groups: Option<Vec<&'a str>>,
    pub artists: Option<Vec<&'a str>>,
    pub characters: Option<Vec<&'a str>>,
}

impl NhentaiGallery {
    #[inline]
    pub fn url(&self) -> String {
        format!("https://nhentai.net/g/{}/", self.id)
    }

    #[inline]
    pub fn thumbnail(&self) -> String {
        format!(
            "https://t.nhentai.net/galleries/{}/thumb.{}",
            self.media_id, self.images.thumbnail.ext
        )
    }

    #[inline]
    pub fn cover(&self) -> String {
        format!(
            "https://t.nhentai.net/galleries/{}/cover.{}",
            self.media_id, self.images.cover.ext
        )
    }

    #[inline]
    pub fn total_pages(&self) -> usize {
        self.images.pages.len()
    }

    #[inline]
    pub fn page(&self, page: usize) -> Option<String> {
        self.images.pages.get(page - 1).map(|v| {
            format!(
                "https://i.nhentai.net/galleries/{}/{}.{}",
                self.media_id, page, v.ext
            )
        })
    }

    #[cfg(feature = "extra")]
    pub fn metadata<'a>(&'a self) -> NhentaiMetadata<'a> {
        use magic::traits::MagicOption as _;
        let mut metadata = NhentaiMetadata::default();

        for tag in &self.tags {
            match tag.r#type {
                NhentaiType::Category => metadata.categories.push(tag.name.as_str()),
                NhentaiType::Language => metadata.languages.push(tag.name.as_str()),
                NhentaiType::Parody => metadata.parodies.extend_inner(tag.name.as_str()),
                NhentaiType::Tag => metadata.tags.extend_inner(tag.name.as_str()),
                NhentaiType::Group => metadata.groups.extend_inner(tag.name.as_str()),
                NhentaiType::Artist => metadata.artists.extend_inner(tag.name.as_str()),
                NhentaiType::Character => metadata.characters.extend_inner(tag.name.as_str()),
            }
        }

        metadata
    }
}

type StdResult<T, E> = std::result::Result<T, E>;
fn string_or_u64<'de, D: Deserializer<'de>>(deserializer: D) -> StdResult<u64, D::Error> {
    struct StringOrU64;
    impl<'de> Visitor<'de> for StringOrU64 {
        type Value = u64;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            write!(formatter, "string or u64")
        }

        fn visit_str<E: de::Error>(self, value: &str) -> StdResult<Self::Value, E> {
            value.parse().map_err(de::Error::custom)
        }

        fn visit_u64<E: de::Error>(self, value: u64) -> StdResult<Self::Value, E> {
            Ok(value)
        }
    }

    deserializer.deserialize_any(StringOrU64)
}

pub struct NhentaiSearch {
    pub results: Vec<NhentaiSearchItem>,
    pub total_pages: usize,
}

pub struct NhentaiSearchItem {
    pub id: u64,
    pub media_id: u64,
}

impl Into<u64> for NhentaiSearchItem {
    fn into(self) -> u64 {
        self.id
    }
}

struct SearchSelector {
    galleries: Selector,
    id: Selector,
    media_id: Selector,
}

impl Default for SearchSelector {
    fn default() -> Self {
        Self {
            galleries: Selector::parse(".gallery").unwrap(),
            id: Selector::parse(".cover").unwrap(),
            media_id: Selector::parse(".lazyload").unwrap(),
        }
    }
}

impl SearchSelector {
    fn extract_info<'a>(&'a self, html: &'a Html) -> impl Iterator<Item = NhentaiSearchItem> + 'a {
        html.select(&self.galleries).filter_map(move |e| {
            let id = self.extract_id(&e)?;
            let media_id = self.extract_media_id(&e)?;

            Some(NhentaiSearchItem { id, media_id })
        })
    }

    fn extract_id(&self, e: &ElementRef<'_>) -> Option<u64> {
        let attr = e.select(&self.id).next()?.value().attr("href")?;
        attr.split('/').nth(2)?.parse().ok()
    }

    fn extract_media_id(&self, e: &ElementRef<'_>) -> Option<u64> {
        let attr = e.select(&self.media_id).next()?.value().attr("data-src")?;
        attr.rsplit('/').nth(1)?.parse().ok()
    }
}

#[async_trait]
pub trait NhentaiScraper: Scraper {
    async fn search(&self, text: &str) -> Result<NhentaiSearch> {
        let url = url::Url::parse_with_params("https://nhentai.net/search/", &[("q", text)])?;
        let data = self.html(url.as_ref()).await?;
        let selector = SearchSelector::default();
        let results = selector.extract_info(&data).collect::<Vec<_>>();

        Ok(NhentaiSearch {
            results,
            total_pages: 0,
        })
    }

    async fn gallery(&self, id: Option<u64>) -> Result<Option<NhentaiGallery>> {
        let url = match id {
            Some(id) => format!("https://nhentai.net/g/{}/", id),
            None => "https://nhentai.net/random/".to_string(),
        };

        let text = self.text(&url).await?;
        let data = text
            .lines()
            .rev()
            .find(|v| v.trim_start().starts_with("window._gallery"))
            .and_then(|v| {
                let start = v.find('{')?;
                let end = v.rfind('}')?;
                v.get(start..=end)
            })
            .map(|v| v.replace("\\u0022", "\""));

        match data {
            Some(data) => Ok(Some(serde_json::from_str(&data)?)), // I want to return if error here...
            None => Ok(None),
        }
    }

    #[inline]
    async fn gallery_by_id<ID>(&self, id: ID) -> Result<Option<NhentaiGallery>>
    where
        ID: Into<u64> + Send,
    {
        self.gallery(Some(id.into())).await
    }

    #[inline]
    async fn random(&self) -> Result<NhentaiGallery> {
        self.gallery(None)
            .await
            .and_then(|v| v.ok_or_else(|| Box::new(magic::Error) as Box<_>))
    }
}

impl<S: Scraper + ?Sized> NhentaiScraper for S {}

#[cfg(all(feature = "serenity-ext", feature = "extra"))]
mod serenity_ext_impl {
    use super::NhentaiGallery;
    use chrono::prelude::*;
    use core::num::NonZeroUsize;
    use serenity_ext::*;

    const NHENTAI_ICON: &str =
        "https://cdn.discordapp.com/attachments/513304527064006656/766670086928859146/nhen.png";

    impl Embedable for NhentaiGallery {
        fn append(&self, embed: &mut CreateEmbed) {
            let metadata = self.metadata();
            let mut description = format!(
                "**Category**: {}\n**Language**: {}\n**Total Pages**: {}\n",
                metadata.categories.join(", "),
                metadata.languages.join(", "),
                self.total_pages(),
            );

            if !self.scanlator.is_empty() {
                let data = format!("**Scanlator**: {}\n", &self.scanlator);
                description.push_str(&data);
            }

            if let Some(parodies) = metadata.parodies {
                let data = format!("**Parody**: {}\n", parodies.join(", "));
                description.push_str(&data);
            }

            if let Some(characters) = metadata.characters {
                let data = format!("**Character**: {}\n", characters.join(", "));
                description.push_str(&data);
            }

            if let Some(groups) = metadata.groups {
                let data = format!("**Group**: {}\n", groups.join(", "));
                description.push_str(&data);
            }

            if let Some(artists) = metadata.artists {
                let data = format!("**Artist**: {}\n", artists.join(", "));
                description.push_str(&data);
            }

            let color = {
                let num_length = (self.id as f32 + 1.0).log10().ceil() as u64;
                self.media_id * num_length + self.id
            };

            embed.title(&self.title.pretty);
            embed.url(self.url());
            embed.thumbnail(self.thumbnail());
            embed.description(description);
            embed.color(color & 0xffffff);
            embed.timestamp(Utc.timestamp(self.upload_date as _, 0).to_rfc3339());
            embed.footer(|f| f.text(format!("ID: {}", self.id)).icon_url(NHENTAI_ICON));

            if let Some(tags) = metadata.tags {
                embed.field("Tags", tags.join(", "), false);
            }
        }
    }

    impl Paginator for NhentaiGallery {
        fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed) {
            let total_pages = self.total_pages();
            let page = page.get();
            let color = {
                let num_length = (self.id as f32 + 1.0).log10().ceil() as u64;
                self.media_id * num_length + self.id
            };

            embed.title(&self.title.pretty);
            embed.url(self.url());
            embed.color(color);

            embed.footer(|f| {
                f.text(format!(
                    "ID: {} | Page: {} / {}",
                    self.id, page, total_pages
                ))
                .icon_url(NHENTAI_ICON)
            });

            match self.page(page) {
                Some(p) => embed.image(p),
                None => embed.field(
                    "Error",
                    format!("Out of page, this gallery has only {} pages", total_pages),
                    false,
                ),
            };
        }

        fn total_pages(&self) -> Option<usize> {
            Some(self.total_pages())
        }
    }
}
