use crate::{Requester, Result};

pub enum ApiType {
    MoeBooru,
    DanBooru,
}

pub trait MoeBooru: Requester {
    const ENDPOINT: &'static str;
    const API_TYPE: ApiType;
    type Data;
    
    async fn get(tags, page, limit) -> Result<Vec<Self::Data>> {
        let url = match Self::API_TYPE {
            ApiType::MoeBooru => format!("{}&"),
            ApiType::DanBooru => todo!(),
        };
    }
}