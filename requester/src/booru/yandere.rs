use crate::{Requester, Result};

#[derive(Debug, Serialize, Deserialize)]
pub struct Yandere {
    pub id:	u64,
    pub tags: String,
    pub created_at:	usize,
    pub author: String,
    pub source:	Option<String>,
    pub score: usize,
    pub md5: String,
    pub file_size: usize,
    pub file_ext: String,
    pub file_url: String,
    pub rating: char,
    pub width: usize,
    pub height:	usize,
}

#[async_trait]
pub trait YandereApi: Requester {
    async fn list_post(tags: &str, page: u32, limut: u8) -> Result<Yandere> {
        let url = 
        Ok()
    }
}

impl<R: Requester> YandereApi for R {}