#[macro_use]
extern crate async_trait;

#[macro_use]
extern crate serde;

use futures::io::{AsyncRead, AsyncReadExt};
use futures::stream::TryStreamExt;
use serde::de::DeserializeOwned;
use serde::ser;
use std::collections::HashMap;

pub mod currency;
pub mod ehentai;
pub mod mazii;
pub mod r18;
pub mod smogon;
pub mod tenor;
pub mod urban;

pub use currency::CurrencyApi;
pub use ehentai::EhentaiApi;
pub use mazii::MaziiApi;
pub use r18::R18Api;
pub use smogon::SmogonApi;
pub use tenor::TenorApi;
pub use urban::UrbanApi;

#[cfg(feature = "scrapers")]
pub mod duckduckgo;
#[cfg(feature = "scrapers")]
pub mod google;
#[cfg(feature = "scrapers")]
pub mod nhentai;
#[cfg(feature = "scrapers")]
pub mod saucenao;

#[cfg(feature = "scrapers")]
pub use duckduckgo::DuckDuckGoScraper;
#[cfg(feature = "scrapers")]
pub use google::GoogleScraper;
#[cfg(feature = "scrapers")]
pub use nhentai::NhentaiScraper;
#[cfg(feature = "scrapers")]
pub use r18::R18Scraper;
#[cfg(feature = "scrapers")]
pub use saucenao::SauceNaoScraper;

#[cfg(feature = "reqwest")]
pub use reqwest::get;
#[cfg(feature = "reqwest")]
pub use reqwest::Client as Reqwest;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Sync + Send>>;
const FIREFOX_AGENT: &str = "Mozilla/5.0 (X11; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0";

#[derive(Debug, Clone)]
pub struct Content<'a> {
    pub method: Method,
    pub body: Option<String>,
    pub headers: HashMap<&'a str, &'a str>,
}

impl Default for Content<'_> {
    fn default() -> Self {
        let mut headers = HashMap::new();
        headers.insert("user-agent", FIREFOX_AGENT);

        Self {
            method: Method::Get,
            body: None,
            headers,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Method {
    Get,
    Post,
}

impl Default for Method {
    fn default() -> Self {
        Self::Get
    }
}

type AsyncResponse = dyn AsyncRead + Unpin + Send;

#[async_trait]
pub trait HttpClient: Sync {
    async fn execute(&self, url: &str, data: Content<'_>) -> Result<Box<AsyncResponse>>;

    async fn bytes(&self, url: &str, data: Content<'_>) -> Result<Vec<u8>> {
        let mut res = Vec::new();
        self.execute(url, data).await?.read_to_end(&mut res).await?;
        Ok(res)
    }

    async fn text(&self, url: &str, mut data: Content<'_>) -> Result<String> {
        if !data.headers.contains_key("content-type") {
            data.headers.insert("content-type", "text/plain");
        }

        let mut res = String::new();
        self.execute(url, data)
            .await?
            .read_to_string(&mut res)
            .await?;
        Ok(res)
    }

    async fn json<D: DeserializeOwned>(&self, url: &str, mut data: Content<'_>) -> Result<D> {
        if data
            .headers
            .get("content-type")
            .filter(|&&v| v == "application/json")
            .is_none()
        {
            data.headers.insert("content-type", "application/json");
        }

        let bytes = self.bytes(url, data).await?;
        serde_json::from_slice(&bytes).map_err(|e| Box::new(e) as Box<_>)
    }
}

#[async_trait]
pub trait Requester: Sync {
    async fn get<D: DeserializeOwned>(&self, url: &str) -> Result<D>;
    async fn post<D: DeserializeOwned, B: ser::Serialize + Sync + ?Sized>(
        &self,
        url: &str,
        body: &B,
    ) -> Result<D>;
}

#[cfg(feature = "scrapers")]
#[async_trait]
pub trait Scraper {
    async fn text(&self, url: &str) -> Result<String>;

    async fn html(&self, url: &str) -> Result<scraper::Html> {
        let text = self.text(url).await?;
        Ok(scraper::Html::parse_document(&text))
    }

    async fn html_fragment(&self, url: &str) -> Result<scraper::Html> {
        let text = self.text(url).await?;
        Ok(scraper::Html::parse_fragment(&text))
    }
}

#[async_trait]
impl<H: HttpClient> Requester for H {
    #[inline]
    async fn get<D: DeserializeOwned>(&self, url: &str) -> Result<D> {
        self.json(url, Default::default()).await
    }

    async fn post<D: DeserializeOwned, B: ser::Serialize + Sync + ?Sized>(
        &self,
        url: &str,
        body: &B,
    ) -> Result<D> {
        let content = Content {
            body: Some(serde_json::to_string(body)?),
            method: Method::Post,
            ..Default::default()
        };

        self.json(url, content).await
    }
}

#[cfg(feature = "scrapers")]
#[async_trait]
impl<H: HttpClient> Scraper for H {
    async fn text(&self, url: &str) -> Result<String> {
        let mut content = Content::default();
        content.headers.insert("content-type", "text/html");

        self.text(url, content).await
    }
}

#[cfg(feature = "reqwest_client")]
#[async_trait]
impl HttpClient for reqwest::Client {
    async fn execute(&self, url: &str, data: Content<'_>) -> Result<Box<AsyncResponse>> {
        use reqwest::header::HeaderName;
        use std::io::{Error, ErrorKind};

        let method = match data.method {
            Method::Get => reqwest::Method::GET,
            Method::Post => reqwest::Method::POST,
        };

        let mut request = reqwest::Request::new(method, url.parse()?);
        *request.body_mut() = data.body.map(|v| v.into());

        for (k, v) in data.headers {
            request
                .headers_mut()
                .insert(k.parse::<HeaderName>()?, v.parse()?);
        }

        let response = self
            .execute(request)
            .await?
            .bytes_stream()
            .map_err(|e| Error::new(ErrorKind::Interrupted, e))
            .into_async_read();

        Ok(Box::new(response) as Box<_>)
    }
}

#[cfg(feature = "yukikaze_client")]
#[async_trait]
impl HttpClient for yukikaze::client::Client {
    async fn execute(&self, url: &str, data: Content<'_>) -> Result<Box<AsyncResponse>> {
        use std::io::{Error, ErrorKind};
        use yukikaze::header::HeaderName;

        let method = match data.method {
            Method::Get => "GET".parse().unwrap(),
            Method::Post => "POST".parse().unwrap(),
        };

        let mut request = yukikaze::client::Request::new(method, url)?.body(data.body);

        for (k, v) in data.headers {
            request
                .headers_mut()
                .insert(k.parse::<HeaderName>()?, v.parse()?);
        }

        let response = self
            .request(request)
            .await?
            .extract_body()
            .2
            .map_err(|e| Error::new(ErrorKind::Interrupted, e))
            .into_async_read();

        Ok(Box::new(response) as Box<_>)
    }
}
