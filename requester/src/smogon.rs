use crate::Result;
use core::convert::TryFrom;
use core::str::FromStr;
use std::collections::HashMap;

type DumpPokemonResult = Result<SmogonPokemon>;
type StrategyResult = Result<Vec<SmogonStrategy>>;

const BASICS_ENDPOINT: &str = "https://www.smogon.com/dex/_rpc/dump-basics";
const STRATEGY_ENDPOINT: &str = "https://www.smogon.com/dex/_rpc/dump-pokemon";
const ABILITY_ENDPOINT: &str = "https://www.smogon.com/dex/_rpc/dump-ability";
const MOVE_ENDPOINT: &str = "https://www.smogon.com/dex/_rpc/dump-move";
const ITEM_ENDPOINT: &str = "https://www.smogon.com/dex/_rpc/dump-item";

#[derive(Debug, Deserialize, Serialize)]
pub struct DumpBasics {
    pub pokemon: Vec<Pokemon>,
    pub formats: Vec<Format>,
    pub natures: Vec<Nature>,
    pub abilities: Vec<Ability>,
    // pub moveflags: Vec<_>,
    pub moves: Vec<Move>,
    pub types: Vec<Type>,
    pub items: Vec<Item>,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct Pokemon {
    pub name: String,
    pub hp: u8,
    pub atk: u8,
    pub def: u8,
    pub spa: u8,
    pub spd: u8,
    pub spe: u8,
    pub weight: f32,
    pub height: f32,
    pub types: Vec<String>,
    pub abilities: Vec<String>,
    pub format: Option<Vec<String>>,
    #[serde(rename = "isNonstandard")]
    pub is_nonestandard: String,
    pub oob: Option<OOB>,
}

impl Pokemon {
    pub fn base_stats_total(&self) -> u16 {
        self.hp as u16
            + self.atk as u16
            + self.def as u16
            + self.spa as u16
            + self.spd as u16
            + self.spe as u16
    }

    pub fn dex_number(&self) -> Option<u16> {
        self.oob
            .as_ref()
            .and_then(|v| u16::try_from(v.dex_number).ok())
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OOB {
    pub dex_number: i16,
    pub evos: Vec<String>,
    pub alts: Vec<String>,
    pub genfamily: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Format {
    pub name: String,
    pub shorthand: String,
    pub genfamily: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Nature {
    pub name: String,
    pub hp: f32,
    pub atk: f32,
    pub def: f32,
    pub spa: f32,
    pub spd: f32,
    pub spe: f32,
    pub summary: String,
    pub genfamily: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Ability {
    pub name: String,
    pub description: String,
    #[serde(rename = "isNonstandard")]
    pub is_nonestandard: String,
    pub genfamily: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Move {
    pub name: String,
    #[serde(rename = "isNonstandard")]
    pub is_nonstandard: String,
    pub category: String,
    pub power: u8,
    pub accuracy: u8,
    pub priority: i8,
    pub pp: u8,
    pub description: String,
    #[serde(rename = "type")]
    pub kind: String,
    // pub flags: Vec<_>,
    pub genfamily: Vec<Generation>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Type {
    pub atk_effectives: Vec<(String, f32)>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Item {
    pub name: String,
    pub description: String,
    #[serde(rename = "isNonstandard")]
    pub is_nonestandard: String,
    pub genfamily: Vec<Generation>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SmogonPokemon {
    pub languages: Vec<String>,
    pub learnset: Vec<String>,
    pub strategies: Vec<SmogonStrategy>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SmogonStrategy {
    pub format: String,
    pub overview: String,
    pub comments: String,
    pub movesets: Vec<MoveSet>,
    pub credits: Option<Credit>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SmogonCommon {
    pub description: String,
    pub pokemon: Option<Vec<String>>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Credit {
    pub teams: Vec<TeamInfo>,
    #[serde(alias = "writtenBy")]
    pub written_by: Vec<MemberInfo>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct TeamInfo {
    pub name: String,
    pub members: Vec<MemberInfo>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct MemberInfo {
    pub user_id: u32,
    pub username: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct MoveSet {
    pub name: String,
    pub pokemon: String,
    // pub level: u8,
    pub description: String,
    pub abilities: Vec<String>,
    pub items: Vec<String>,
    pub moveslots: Vec<Vec<MoveType>>,
    pub evconfigs: Vec<StatsConfig>,
    pub ivconfigs: Vec<StatsConfig>,
    pub natures: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct MoveType {
    #[serde(rename = "move")]
    pub name: String,
    #[serde(rename = "type")]
    pub typ: Option<String>,
}

type StatsConfig = HashMap<String, u8>;

#[derive(Debug, Deserialize, Serialize, Clone, Copy, PartialEq)]
pub enum Generation {
    #[serde(rename = "ss")]
    #[serde(alias = "SS")]
    SwordShield,

    #[serde(rename = "sm")]
    #[serde(alias = "SM")]
    SunMoon,

    #[serde(rename = "xy")]
    #[serde(alias = "XY")]
    XY,

    #[serde(rename = "bw")]
    #[serde(alias = "BW")]
    BlackWhite,

    #[serde(rename = "dp")]
    #[serde(alias = "DP")]
    DiamondPearl,

    #[serde(rename = "rs")]
    #[serde(alias = "RS")]
    RubySapphire,

    #[serde(rename = "gs")]
    #[serde(alias = "GS")]
    GoldSilver,

    #[serde(rename = "rb")]
    #[serde(alias = "RB")]
    RedBlue,
}

impl Generation {
    pub fn shorthand(&self) -> &'static str {
        match self {
            Self::SwordShield => "ss",
            Self::SunMoon => "sm",
            Self::XY => "xy",
            Self::BlackWhite => "bw",
            Self::DiamondPearl => "dp",
            Self::RubySapphire => "rs",
            Self::GoldSilver => "gs",
            Self::RedBlue => "rb",
        }
    }
}

impl Default for Generation {
    fn default() -> Self {
        Self::SwordShield
    }
}

pub struct ParseGenerationError;

impl FromStr for Generation {
    type Err = ParseGenerationError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s.trim().to_lowercase().as_str() {
            "ss" => Ok(Self::SwordShield),
            "sm" => Ok(Self::SunMoon),
            "xy" => Ok(Self::XY),
            "bw" => Ok(Self::BlackWhite),
            "dp" => Ok(Self::DiamondPearl),
            "rs" => Ok(Self::RubySapphire),
            "gs" => Ok(Self::GoldSilver),
            "rb" => Ok(Self::RedBlue),
            _ => Err(ParseGenerationError),
        }
    }
}

use std::fmt;

impl fmt::Display for Generation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::SwordShield => "Sword/Shield",
            Self::SunMoon => "Sun/Moon",
            Self::XY => "X/Y",
            Self::BlackWhite => "Black/White",
            Self::DiamondPearl => "Diamond/Pearl",
            Self::RubySapphire => "Ruby/Sapphire",
            Self::GoldSilver => "Gold/Silver",
            Self::RedBlue => "Red/Blue",
        };

        write!(f, "{}", s)
    }
}

// #[derive(Deserialize)]
// pub struct StatsConfig {
//     pub hp: u8,
//     pub atk: u8,
//     pub def: u8,
//     pub spa: u8,
//     pub spd: u8,
//     pub spe: u8,
// }

#[cfg(feature = "extra")]
impl MoveSet {
    #[inline]
    pub fn ability(&self) -> String {
        self.abilities.join(" / ")
    }

    #[inline]
    pub fn nature(&self) -> String {
        self.natures.join(" / ")
    }

    pub fn ev_config(&self) -> Vec<String> {
        use magic::traits::MagicIter;

        self.evconfigs
            .iter()
            .map(|stats| {
                stats
                    .iter()
                    .filter(|(_, value)| **value > 0)
                    .map(|(k, v)| stats_display(&k.to_owned(), *v))
                    .join(" / ")
            })
            .collect()
    }

    pub fn iv_config(&self) -> Vec<String> {
        use magic::traits::MagicIter;

        self.ivconfigs
            .iter()
            .map(|stats| {
                stats
                    .iter()
                    .filter(|(_, value)| **value < 31)
                    .map(|(k, v)| stats_display(&k.to_owned(), *v))
                    .join(" / ")
            })
            .collect()
    }
}

#[cfg(feature = "extra")]
fn stats_display(stats: &str, value: u8) -> String {
    let k = match stats {
        "hp" => "HP",
        "atk" => "Attack",
        "def" => "Defend",
        "spa" => "Special Attack",
        "spd" => "Special Defend",
        "spe" => "Speed",
        _ => "Unknown stats",
    };

    format!("{} {}", value, k)
}

#[async_trait]
pub trait SmogonApi: crate::Requester {
    async fn dump_basics(&self, gen: Generation) -> Result<DumpBasics> {
        #[derive(Serialize)]
        struct DumpBasicsBody {
            gen: Generation,
        }

        let params = DumpBasicsBody { gen };
        self.post(BASICS_ENDPOINT, &params).await
    }

    async fn dump_pokemon(&self, pokemon: &str, gen: Generation) -> DumpPokemonResult {
        #[derive(Serialize)]
        struct DumpPokemonBody<'a> {
            gen: Generation,
            alias: &'a str,
            language: &'a str,
        }

        let params = DumpPokemonBody {
            gen,
            alias: pokemon,
            language: "en",
        };

        self.post(STRATEGY_ENDPOINT, &params).await
    }

    /// Simple route for ability and move
    async fn dump_common(&self, url: &str, data: &str, gen: Generation) -> Result<SmogonCommon> {
        #[derive(Serialize)]
        struct DumpCommonBody {
            gen: Generation,
            alias: String,
        }

        let params = DumpCommonBody {
            gen,
            alias: normalize_name(data),
        };

        self.post(url, &params).await
    }

    async fn dump_ability(&self, data: &str, gen: Generation) -> Result<SmogonCommon> {
        self.dump_common(ABILITY_ENDPOINT, data, gen).await
    }

    async fn dump_move(&self, data: &str, gen: Generation) -> Result<SmogonCommon> {
        self.dump_common(MOVE_ENDPOINT, data, gen).await
    }

    async fn dump_item(&self, data: &str, gen: Generation) -> Result<SmogonCommon> {
        self.dump_common(ITEM_ENDPOINT, data, gen).await
    }

    async fn strategy(&self, pokemon: &str, gen: Generation) -> StrategyResult {
        let result = self.dump_pokemon(pokemon, gen).await?;
        Ok(result.strategies)
    }
}

impl<R: crate::Requester + ?Sized> SmogonApi for R {}

fn normalize_name(name: &str) -> String {
    name.to_lowercase()
        .split_whitespace()
        .collect::<Vec<_>>()
        .join("-")
}
