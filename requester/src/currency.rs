use crate::{Requester, Result};
use std::collections::HashMap;
use url::Url;

const END_POINT: &str = "https://free.currconv.com/api/v7";

#[derive(Deserialize)]
struct ListCurrencies {
    results: HashMap<String, Currency>,
}

#[derive(Deserialize, Serialize)]
pub struct Currency {
    #[serde(rename = "currencyName")]
    pub name: String,
    #[serde(rename = "currencySymbol")]
    pub symbol: Option<String>,
    pub id: String,
}

#[async_trait::async_trait]
pub trait CurrencyApi: Requester {
    async fn currency_rate(&self, from: &str, to: &str, apikey: &str) -> Result<f64> {
        let query = format!("{}_{}", from, to).to_uppercase();
        let url = Url::parse_with_params(
            &format!("{}/convert", END_POINT),
            &[("apiKey", apikey), ("q", &query), ("compact", "ultra")],
        )?;

        let res: HashMap<String, f64> = self.get(url.as_ref()).await?;

        res.into_values()
            .next()
            .ok_or("Cannot find the currency rate".into())
    }

    async fn list_currencies(&self, apikey: &str) -> Result<HashMap<String, Currency>> {
        let url =
            Url::parse_with_params(&format!("{}/currencies", END_POINT), &[("apiKey", apikey)])?;

        self.get::<ListCurrencies>(url.as_ref())
            .await
            .map(|v| v.results)
    }
}

impl<R: Requester + ?Sized> CurrencyApi for R {}
