use std::sync::atomic::{AtomicUsize, Ordering};

use serenity::model::channel::{Attachment, Message};
use serenity::model::id::MessageId;

use crate::utils::{get_file_bytes, log_err};
use crate::Result;
use bytes::Bytes;
use serde::{Deserialize, Serialize};

const MAX_MESSAGE: AtomicUsize = AtomicUsize::new(2000);

pub struct DbCache {
    db: db::TypedDbInstance<u64, MessageCache>,
    max_message: AtomicUsize,
}

/// Another version of the Message struct, but with the fields we actually need for the logger
/// only the message from guild are allowed
/// we don't need to store the channel id nor guild id here, since we have it at the events handler
#[derive(Serialize, Deserialize)]
pub struct MessageCache {
    pub attachments: Vec<AttachmentCache>,
    pub content: String,
    ///we only need the ID, and then fetch them from serenity cache or REST API
    pub author_id: u64,
}

impl From<Message> for MessageCache {
    fn from(msg: Message) -> Self {
        let attachments = msg
            .attachments
            .into_iter()
            .map(AttachmentCache::from)
            .collect();

        Self {
            attachments,
            content: msg.content,
            author_id: msg.author.id.0,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct AttachmentCache {
    pub id: u64,
    pub url: String,
    pub size: u32,
    pub cached: Option<Bytes>,
}

impl From<Attachment> for AttachmentCache {
    fn from(a: Attachment) -> Self {
        Self {
            id: *a.id.as_u64(),
            url: a.url,
            size: a.size as u32,
            cached: None,
        }
    }
}

impl AttachmentCache {
    /// Get the file name of the attachment
    /// This is not the actual filename, just an id of it for caching
    pub fn filename(&self) -> String {
        let ext = self.url.split('.').last().unwrap_or("jpg");
        format!("{}.{}", self.id, ext)
    }
}

impl DbCache {
    /// Create a new custom cache, as well as a cache directory
    /// Default max_message is 2000
    pub fn new(db: db::DbInstance) -> Result<Self> {
        Ok(Self {
            db: db.open("cache")?.into_typed(),
            max_message: MAX_MESSAGE,
        })
    }

    /// Clear the cache
    /// Return the length of messages and cached size on disk
    pub fn clear(&self) -> Result<(usize, usize)> {
        let res = self.db.as_ref().clear()?;
        Ok((res.items_count, res.total_size))
    }

    /// Set new maximum message allow in the cache
    /// return the old value
    pub fn set_max_message(&self, value: usize) -> usize {
        let old_value = self.max_message.swap(value, Ordering::SeqCst);

        if value == 0 {
            log_err("Error while removing messages", self.clear());
            return old_value;
        }

        let len = self.len();

        if len < value {
            let remove_range = len - value;
            log_err(
                "Error while removing messages",
                self.db.as_ref().remove_first_n(remove_range),
            );
        }

        old_value
    }

    fn len(&self) -> usize {
        self.db
            .as_ref()
            .root_db()
            .typed::<String, usize>()
            .get(&"cache_message".to_owned())
            .ok()
            .flatten()
            .unwrap_or(0)
    }

    /// Insert a message to the cache
    pub async fn insert_message(&self, msg: Message) {
        let len = self.len();

        if len >= self.max_message.load(Ordering::Acquire) {
            log_err(
                "Cannot remove a cache message",
                self.db.as_ref().tree().pop_min(),
            );
        }

        let id = msg.id;
        let mut cache_message = MessageCache::from(msg);

        for i in cache_message.attachments.iter_mut() {
            let max_file_size = crate::read_config().await.max_cache_file_size;

            if i.size <= max_file_size {
                i.cached = get_file_bytes(&i.url).await.ok();
            }
        }

        log_err(
            "Cannot cache message to the database",
            self.db.insert(id.as_u64(), &cache_message),
        );

        let update_len = self
            .db
            .as_ref()
            .root_db()
            .typed::<String, usize>()
            .fetch_and_update(&"cache_message".to_owned(), |v| Some(v.unwrap_or(0) + 1));
        log_err("Cannot update len of cache message", update_len);
    }

    /// Update the message content, return the old cached content
    pub async fn update_message(&self, id: MessageId, content: &str) -> Option<String> {
        let content = content.to_owned();

        let old_message = self.db.fetch_and_update(id.as_u64(), move |old| {
            old.map(|mut v| {
                v.content = content.to_owned();
                v
            })
        });

        old_message.ok()?.map(|v| v.content)
    }

    /// Remove the message from cache by a given MessageId
    /// Return the cached message if exist
    pub async fn remove_message<I: Into<MessageId>>(&self, msg: I) -> Option<MessageCache> {
        let id = msg.into();
        self.db.remove(id.as_u64()).ok().flatten()
    }
}
