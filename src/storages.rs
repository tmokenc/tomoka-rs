use crate::cache::DbCache;
use crate::music::MusicManager;
use crate::types::*;
use db::DbInstance;
use eliza::Eliza;
use reqwest::Client as Reqwest;
use serenity::prelude::TypeMapKey;
use std::sync::Arc;
use tokio::sync::Mutex;

type MutexData<T> = Arc<Mutex<T>>;

pub struct RawEventList;
impl TypeMapKey for RawEventList {
    type Value = serenity_ext::MultiRawHandler;
}

pub struct InforKey;
impl TypeMapKey for InforKey {
    type Value = Information;
}

pub struct ReqwestClient;
impl TypeMapKey for ReqwestClient {
    type Value = Arc<Reqwest>;
}

pub struct DatabaseKey;
impl TypeMapKey for DatabaseKey {
    type Value = DbInstance;
}

pub struct ReminderNotify;
impl TypeMapKey for ReminderNotify {
    type Value = Arc<tokio::sync::Notify>;
}

pub struct MusicKey;
impl TypeMapKey for MusicKey {
    type Value = MusicManager;
}

pub struct CacheStorage;
impl TypeMapKey for CacheStorage {
    type Value = Arc<DbCache>;
}

pub struct AIStore;
impl TypeMapKey for AIStore {
    type Value = MutexData<Eliza>;
}
