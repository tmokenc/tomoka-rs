mod event;
mod manager;
mod player;
mod song;

pub use event::MusicEventHandler;
pub use manager::MusicManager;
pub use player::MusicPlayer;
pub use song::Song;

use player::{ListDbStore, ListId, PlayState};

use crate::storages::DatabaseKey;
use crate::utils;
use crate::utils::{get_data, log_err};
use chrono::Utc;
use core::time::Duration;
use serenity::prelude::Context;

pub async fn get_music_db(ctx: &Context) -> Option<db::TypedDbInstance<ListId, ListDbStore>> {
    get_data::<DatabaseKey>(ctx)
        .await
        .and_then(|v| v.open("musicList").ok())
        .map(|v| v.into_typed::<ListId, ListDbStore>())
}

pub async fn music_care_service(manager: MusicManager) {
    let db = get_music_db(&*manager.ctx).await.expect("Music databasse");
    let mut next_db_cleaning_on = Utc::now();

    loop {
        let current_time = Utc::now();
        let mut dead_guilds = Vec::new();
        let player_list = manager.manager.read().await;

        for (guild, player_lock) in player_list.iter() {
            let player = player_lock.lock().await;

            if utils::is_dead_channel(&*manager.ctx, player.channel).await {
                dead_guilds.push(*guild);
                continue;
            }

            match player.state {
                PlayState::Paused(_, instant) | PlayState::Stopped(instant) => {
                    let duration = instant.elapsed();

                    if duration.as_secs() > 60 * 15 {
                        dead_guilds.push(*guild);
                        continue;
                    }
                }

                _ => (),
            }
        }

        drop(player_list);

        if !dead_guilds.is_empty() {
            let mut player_list_mut = manager.manager.write().await;

            for guild in dead_guilds {
                if let Some(player) = player_list_mut.remove(&guild) {
                    player
                        .lock()
                        .await
                        .connection
                        .send_message("Leaving due to inactive");
                }
            }
        }

        let need_db_cleaning = next_db_cleaning_on <= current_time;

        if need_db_cleaning {
            next_db_cleaning_on = Utc::now() + chrono::Duration::days(3);

            for (key, value) in db.iter() {
                let time_since_last = current_time.signed_duration_since(value.last_access);

                if time_since_last.num_days() >= 3 {
                    log::info!("Removing music list {} from db", &key);
                    log_err("Cannot remove data", db.remove(&key));
                } else {
                    let delete_on = value.last_access.to_owned() + chrono::Duration::days(3);

                    if next_db_cleaning_on > delete_on {
                        next_db_cleaning_on = delete_on;
                    }
                }
            }
        }

        // let wait = Duration::from_millis(next_wait_time.take().unwrap_or(10 * 60 * 1000));
        let wait = Duration::from_secs(60 * 10);
        log::info!("Next wait of music care service: {:?}", wait);
        tokio::time::sleep(wait).await;
    }
}
