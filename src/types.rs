#![allow(unstable_name_collisions)]

use crate::Result;
use chrono::{DateTime, Utc};
use core::ops::{Deref, DerefMut};
use core::time::Duration;
use serde::{Deserialize, Serialize};
use serenity::builder::CreateEmbed;
use serenity::client::Context;
use serenity::http::client::Http;
use serenity::model::channel::Message;
use serenity::model::id::{UserId};
use serenity_ext::Embedable;
use std::borrow::Borrow;
use std::sync::atomic::{AtomicUsize, Ordering};

use magic::traits::MagicStr as _;

pub use common::GuildConfig;

pub(crate) struct Ref<T>(pub T);

impl<T> From<T> for Ref<T> {
    fn from(s: T) -> Self {
        Self(s)
    }
}

impl<T: Clone> Clone for Ref<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> Deref for Ref<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Ref<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct Information {
    pub booted_on: DateTime<Utc>,
    pub user_id: UserId,
    pub executed: AtomicUsize,
}

impl Information {
    pub async fn init(http: &Http) -> Result<Self> {
        let info = Self {
            booted_on: Utc::now(),
            user_id: http.get_current_user().await?.id,
            executed: AtomicUsize::new(0),
        };

        Ok(info)
    }

    #[inline]
    pub fn executed_commands(&self) -> usize {
        self.executed.load(Ordering::SeqCst)
    }

    #[inline]
    pub fn executed_one(&self) -> usize {
        self.executed.fetch_add(1, Ordering::SeqCst)
    }

    pub fn uptime(&self) -> Duration {
        let current = Utc::now().timestamp_millis() as u64;
        let since = self.booted_on.timestamp_millis() as u64;

        Duration::from_millis(current - since)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Reminder {
    pub(crate) user_id: u64,
    pub(crate) msg_id: u64,
    pub(crate) channel_id: u64,
    pub(crate) guild_id: Option<u64>,
    pub(crate) content: Option<String>,
    pub when: DateTime<Utc>,
    pub duration: Duration,
}

impl Reminder {
    pub fn new(msg: &Message, duration: Duration, content: &str) -> Self {
        Self {
            user_id: msg.author.id.0,
            msg_id: msg.id.0,
            channel_id: msg.channel_id.0,
            guild_id: msg.guild_id.map(|v| v.0),
            content: content.to_option().map(String::from),
            when: Utc::now(),
            duration,
        }
    }

    pub async fn remind<C: Borrow<Context>>(&self, ctx: C) -> Result<()> {
        let ctx = ctx.borrow();
        let user = UserId(self.user_id);
        let channel = user.create_dm_channel(ctx).await?;

        channel
            .send_message(ctx, |m| m.embed(|embed| self.append_to(embed)))
            .await?;

        Ok(())
    }
}

impl Embedable for Reminder {
    fn append(&self, embed: &mut CreateEmbed) {
        let guild = self
            .guild_id
            .map(|v| v.to_string())
            .unwrap_or_else(|| String::from("@me"));
        let dur = humantime::format_duration(self.duration);
        let url = format!(
            "https://discord.com/channels/{}/{}/{}",
            guild, self.channel_id, self.msg_id
        );

        let msg = format!("Just remind you that you have a [reminder]({url}) from {dur} ago");
        let color = self.when.timestamp() as u64 & 0xffffff;

        embed.title(":alarm_clock: REMINDERRRR!!!");
        embed.description(msg);
        embed.color(color);
        embed.image("https://cdn.discordapp.com/attachments/450521152272728065/708817978594033804/Diancie.gif");

        if let Some(mess) = &self.content {
            embed.field("Message", mess, false);
        }

        let when = format!("<t:{}>", self.when.timestamp());
        embed.field("From", when, true);
    }
}