use crate::commands::prelude::*;
use serenity_ext::ChannelExt as _;

#[command]
#[aliases("setcachesize, resizecache, resize_cache")]
#[num_args(1)]
#[owners_only]
async fn set_cache_size(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let size = args.single::<usize>()?;

    let cache = get_data::<CacheStorage>(&ctx).await.unwrap();
    let old_size = cache.set_max_message(size);

    msg.channel_id
        .send_embed(ctx)
        .with_description("The maximum number of message to be cached has been updated!!!")
        .with_field("Old value", old_size, true)
        .with_field("New value", size, true)
        .with_color(0x44eabe)
        .with_current_timestamp()
        .await?;

    Ok(())
}
