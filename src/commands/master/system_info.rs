use crate::commands::prelude::*;
use serenity_ext::ChannelExt;
use magic::{report_kb, report_bytes};
use sysinfo::*;
use tokio::sync::Mutex;
use tokio::task::spawn_blocking;
use chrono::prelude::*;
use std::time::Duration;
use libc::{sysconf, _SC_PAGESIZE};

lazy_static::lazy_static! {
    static ref SYSTEM: Mutex<System> = Mutex::new({
        System::new_with_specifics(RefreshKind::everything()
            .without_processes()
            .without_users_list()
        )
    });
    static ref PAGE_SIZE_KB: u64 = unsafe {
        sysconf(_SC_PAGESIZE) as u64 / 1024
    };
}

struct StatFile {
    rss: u64,
    vsize: u64,
    num_thread: u64,
}

async fn parse_stat(pid: u32) -> Option<StatFile> {
    let path = format!("/proc/{}/stat", pid);
    let file = fs::read_to_string(path).await.ok()?;
    
    let mut iter_data = file.rsplitn(2, ")").next()?.split_whitespace();
    let num_thread = iter_data.nth(17)?.parse().ok()?;
    let vsize = iter_data.nth(2)?.parse().ok()?;
    let rss = iter_data.next()?.parse().ok()?;
    
    Some(StatFile {
        rss, vsize, num_thread,
    })
}

#[command]
#[aliases("systeminfo", "sys_info", "sysinfo")]
#[owners_only]
/// Get the information of the system that I'm running on.
async fn system_info(ctx: &Context, msg: &Message) -> CommandResult {
    let sys = SYSTEM.lock().await;
    let system = spawn_blocking(move || {
        let mut system = sys;
        system.refresh_all();
        system
    }).await?;
    
    let mut send_embed = msg
        .channel_id
        .send_embed(ctx)
        .with_title("System Information")
        .with_color(crate::read_config().await.color.information)
        .with_field("Host", host(&system), true)
        .with_field("CPU", cpu(&system), true)
        .with_field("Memory", mem(&system), true);
        
    if let Some(disk) = disk(&system) {
        send_embed.field("Disk", disk, true);
    }
    
    send_embed.field("Network (last refresh)", network(&system), true);
    send_embed.field("Uptime", up_time(&system), true);
    
    drop(system);
    
    if let Some(usage) = parse_stat(std::process::id()).await {
        send_embed.description(format!(
            "***MY USAGE***\n**Memory**: {}\n**Virtual Memory**: {}\n**Threads**: {}",
            report_kb(usage.rss * *PAGE_SIZE_KB),
            report_kb(usage.vsize),
            usage.num_thread,
        ));
    }
    
    send_embed.await?;

    Ok(())
}

fn host(system: &System) -> String {
    format!(
        "{name} {host_name}\nversion {version}\nkernel {kernel_version}",
        name = system.name().unwrap_or_default(),
        host_name = system.host_name().unwrap_or_default(),
        version = system.os_version().unwrap_or_default(),
        kernel_version = system.kernel_version().unwrap_or_default(),
    )
}

fn cpu(system: &System) -> String {
    format!(
        "{} cores @ {}MHz\nLoad: {}%",
        system.physical_core_count().unwrap_or(1),
        system.global_processor_info().frequency(),
        system.global_processor_info().cpu_usage(),
    )
}

fn disk(system: &System) -> Option<String> {
    let current = std::env::current_dir().unwrap_or_default();
    system.disks()
        .iter()
        .fold(None, |v: Option<&Disk>, x| {
            let path = x.mount_point();
            if !current.starts_with(&path) {
                return v;
            }
            
            match v {
                Some(p) if p.mount_point().iter().count() > path.iter().count() => Some(p),
                _ => Some(x),
            }
        })
        .map(|disk| {
            let free = disk.available_space();
            let total = disk.total_space();
            let used = total - free;
            
            format!(
                "{} / {}\nFree: {} ({}%)",
                report_bytes(used),
                report_bytes(total),
                report_bytes(free),
                free / total,
            )
        })
}

fn network(system: &System) -> String {
    let (up, down) = system.networks().iter().fold((0, 0), |(up, down), x| {
        (up + x.1.transmitted(), down + x.1.received())
    });
    
    format!("Up: {}\nDown: {}", report_bytes(up), report_bytes(down))
}

fn mem(system: &System) -> String {
    let mem_total = system.total_memory();
    let mem_available = system.available_memory();
    let mem_used = mem_total - mem_available;

    let swap_total = system.total_swap();
    let swap_free = system.free_swap();
    let swap_used = system.used_swap();
    
    format!(
        "RAM: {} / {} (free {})\nSwap: {} / {} (free {})",
        report_kb(mem_used),
        report_kb(mem_total),
        report_kb(mem_available),
        report_kb(swap_used),
        report_kb(swap_total),
        report_kb(swap_free),
    )
}

fn up_time(system: &System) -> String {
    let date = Utc.timestamp(system.boot_time() as i64, 0);
    format!(
        "{}\nSince {}", 
        humantime::format_duration(Duration::from_secs(system.uptime())),
        date.format("%X %d/%m/%Y"),
    )
}
