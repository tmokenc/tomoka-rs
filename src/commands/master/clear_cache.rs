use crate::commands::prelude::*;
use serenity_ext::ChannelExt as _;
use magic::report_bytes;

#[command]
#[aliases("clearcache", "cleancache")]
#[owners_only]
/// Clear the __*custom cache*__ for message
/// and then response with a total number of caches has been deleted and total size of files has been deleted on disk (in bytes)
async fn clear_cache(ctx: &Context, msg: &Message) -> CommandResult {
    let cache = get_data::<CacheStorage>(&ctx).await.unwrap();
    let (length, size) = cache.clear()?;
    
    msg.channel_id
        .send_embed(ctx)
        .with_description("Cleared the custom cache")
        .with_field("Message cached", length, true)
        .with_field("Size", report_bytes(size as _), true)
        .with_color(crate::read_config().await.color.information)
        .await?;

    Ok(())
}
