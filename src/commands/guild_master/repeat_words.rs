use serenity::framework::standard::macros::group;
use crate::commands::prelude::*;
use crate::types::GuildConfig;
use serenity_ext::*;
use magic::traits::MagicIter as _;

#[group]
#[prefixes("repeat_word", "repeat_words", "words", "word")]
#[commands(add, remove, enable, disable, toggle, info)]
#[default_command(info)]
struct RepeatWords;

const TITLE: &str = "Repeat-words information";

#[command]
#[only_in(guilds)]
/// Get the info of the repeat-words machine
async fn info(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(())
    };
    
    let config = crate::read_config().await;
    let data = config
        .guilds
        .get(&guild_id);
    
    let mut send_embed = msg.channel_id.send_embed(ctx)
        .with_title(TITLE)
        .with_color(config.color.information)
        .with_current_timestamp();
    
    if let Some(e) = &data {
        e.repeat_words.append_to(send_embed.inner_embed());
    } else {
        send_embed.description("The repeat-words machine doesn't running on this guild yet");
    }
        
    drop(data);
    drop(config);
    send_embed.await?;
    
    Ok(())
}

#[command]
#[min_args(1)]
#[only_in(guilds)]
#[required_permissions(MANAGE_GUILD)]
/// Add (some) word to the repeat list
/// Seperate by `, `
/// These word will be repeated by the bot when someone use it
/// This command will automatically enable the repeat-word machine
async fn add(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let words = args.rest().split(", ").collect::<Vec<_>>();
    if words.get(0).filter(|x| !x.is_empty()).is_none() {
        return Ok(())
    }
    
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(())
    };
    
    let config = crate::read_config().await;
    
    let mut guild = config
        .guilds
        .entry(guild_id)
        .or_insert_with(|| GuildConfig::new(guild_id.0));

    let length = guild.add_words(words);
    
    let color = config.color.information;
    let description = if length > 0 {
        guild.enable_repeat_words();
        update_guild_config(&ctx, &guild).await?;
        
        format!("Added {} words to be repeated", length)
    } else {
        String::from("These words are in the list already")
    };
    
    drop(guild);
    drop(config);

    msg.channel_id
        .send_embed(ctx)
        .with_title(TITLE)
        .with_color(color)
        .with_current_timestamp()
        .with_description(description)
        .await?;
    
    Ok(())
}

#[command]
#[min_args(1)]
#[only_in(guilds)]
#[required_permissions(MANAGE_GUILD)]
/// "Remove words (seperate by `, `) in the repeating words list
async fn remove(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let words: Vec<_> = args.rest().trim().split(", ").collect();
    if words.get(0).filter(|x| !x.is_empty()).is_none() {
        return Ok(())
    }
    
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(())
    };
    
    let config = crate::read_config().await;
    let mut guild = config.guilds.get_mut(&guild_id);
    let color = config.color.information;
    let description = match guild {
        Some(ref mut guild) => {
            let length = guild.remove_words(words);
            
            if length == 0 {
                String::from("There is no word to be removed")
            } else {
                update_guild_config(&ctx, &guild).await?;
                format!("Removed {} words", length)
            }
        }
        
        _ => {
            String::from("This guilds hasn't used this feature yet.")
        }
    };
    
    drop(guild);
    drop(config);
            
    msg.channel_id
        .send_embed(ctx)
        .with_title(TITLE)
        .with_color(color)
        .with_current_timestamp()
        .with_description(description)
        .await?;
   
    Ok(())
}

#[command]
#[only_in(guilds)]
#[required_permissions(MANAGE_GUILD)]
/// Toggle the repeat-words machine on/off
async fn toggle(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(()),
    };
    
    let config = crate::read_config().await;
    let mut guild = config
        .guilds
        .entry(guild_id)
        .or_insert_with(|| GuildConfig::new(guild_id.0));
    
    let current = guild.toggle_repeat_words();
    update_guild_config(&ctx, &guild).await?;
    
    let color = config.color.information;
    
    let (description, words) = match (current, guild.repeat_words.words.len()) {
        (true, 0) => {
            let des = "Enabled the repeat-words machine but there is no word in the list yet
            Consider using the `option words add` command to add words to be repeated";
            (des, None)
        }
        
        (true, _) => {
            let words = guild
            .repeat_words
            .words
            .iter()
            .map(|w| format!("`{}`", w))
            .join(", ");
            
            ("Enabled the repeat-words machine", Some(words))
        } 
        
        _ => {
            ("Disabled the repeat-words machine", None)
        }
    };
    
    drop(guild);
    drop(config);
    
    let mut send_embed = msg.channel_id
        .send_embed(ctx)
        .with_title(TITLE)
        .with_color(color)
        .with_current_timestamp()
        .with_description(description);
        
    if let Some(w) = words {
        send_embed.field("Words", w, true);
    }
    
    send_embed.await?;

    Ok(())
}

#[command]
#[only_in(guilds)]
#[required_permissions(MANAGE_GUILD)]
/// Enable the repeat-word function
async fn enable(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(())
    };
    
    let config = crate::read_config().await;
    let mut guild = config
        .guilds
        .entry(guild_id)
        .or_insert_with(|| GuildConfig::new(guild_id.0));
        
    guild.enable_repeat_words();
    update_guild_config(&ctx, &guild).await?;
    
    let color = config.color.information;
    let (description, words) = if guild.repeat_words.words.is_empty() {
        let mess = "Enabled the repeat-words machine but there is no word in the list yet
        Consider using the `option words add` command to add words to be repeated";
        (mess, None)
    } else {
        let words = guild
            .repeat_words
            .words
            .iter()
            .map(|w| format!("`{}`", w))
            .join(", ");
            
        ("Enabled the repeat-words machine", Some(words))
    };
    
    drop(guild);
    drop(config);
    
    let mut send_embed = msg.channel_id
        .send_embed(ctx)
        .with_title(TITLE)
        .with_color(color)
        .with_current_timestamp()
        .with_description(description);
        
    if let Some(w) = words {
        send_embed.field("Words", w, true);
    }
    
    send_embed.await?;
    
    Ok(())
}

#[command]
#[only_in(guilds)]
#[required_permissions(MANAGE_GUILD)]
/// Disable the repeat-words machine.
async fn disable(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(())
    };
    
    let config = crate::read_config().await;
    let mut guild = config
        .guilds
        .get_mut(&guild_id);
        
    if let Some(ref mut g) = guild {
        g.disable_repeat_words();
        update_guild_config(&ctx, &g).await?;
    }
    
    let color = config.color.information;
    
    drop(guild);
    drop(config);

    msg.channel_id
        .send_embed(ctx)
        .with_title(TITLE)
        .with_color(color)
        .with_current_timestamp()
        .with_description("Disabled the repeat-words machine")
        .await?;
    
    Ok(())   
}