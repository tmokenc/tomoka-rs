use serenity::framework::standard::macros::group;
use crate::commands::prelude::*;
use crate::GuildConfig;
use serenity_ext::ChannelExt;

#[group]
#[prefix = "prefix"]
#[commands(change, clear, info)]
#[default_command(info)]
struct Prefix;

#[command]
#[only_in(guilds)]
/// Check prefix info on this server
async fn info(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(g) => g,
        None => return Ok(()),
    };

    let config = crate::read_config().await;

    let prefix = config
        .guilds
        .get(&guild_id)
        .and_then(|g| g.prefix.to_owned())
        .unwrap_or_else(|| config.prefix.to_owned());

    let color = config.color.information;
    drop(config);

    msg.channel_id
        .send_embed(ctx)
        .with_title("Prefix information")
        .with_color(color)
        .with_current_timestamp()
        .with_description(format!("Current prefix is **__{}__**", prefix))
        .await?;

    Ok(())
}

#[command]
#[aliases("remove", "unset")]
#[only_in(guilds)]
#[required_permissions(MANAGE_GUILD)]
/// Clear the custom prefix if exists
async fn clear(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = match msg.guild_id {
        Some(id) => id,
        None => return Ok(())
    };
    
    let config = crate::read_config().await;
    
    let mut guild = config
        .guilds
        .get_mut(&guild_id);
        
    let color = config.color.information;
    let prefix = config.prefix.to_owned();
    let description = match guild {
        Some(ref mut g) if g.prefix.is_some() => {
            g.remove_prefix();
            update_guild_config(&ctx, &g).await?;
            "Removed the custom prefix"
        }
        
        _ => "Not found any custom prefix tho..."
    };
    
    drop(guild);
    drop(config);
    
    msg.channel_id
        .send_embed(ctx)
        .with_title("Prefix information")
        .with_color(color)
        .with_current_timestamp()
        .with_field("Current default prefix", prefix, true)
        .with_description(description)
        .await?;
    
    Ok(())
}

#[command]
#[aliases("set")]
#[bucket = "basic"]
#[only_in(guilds)]
#[min_args(1)]
#[required_permissions(MANAGE_GUILD)]
///Set a custom prefix instead of the default
async fn change(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let prefix = args.rest();
    if prefix.is_empty() {
        return Ok(());
    }
    
    let guild_id = match msg.guild_id {
        Some(g) => g,
        None => return Ok(())
    };

    let config = crate::read_config().await;
    let mut g = config
        .guilds
        .entry(guild_id)
        .or_insert_with(|| GuildConfig::new(guild_id.0));

    let old_prefix = g.set_prefix(&prefix);
    let color = config.color.information;
    
    update_guild_config(&ctx, &g).await?;
    drop(g);
    drop(config);
    
    let description = format!("Changed the current prefix to **__{}__**", &prefix);
    
    let mut send_embed = msg.channel_id
        .send_embed(ctx)
        .with_color(color)
        .with_current_timestamp()
        .with_title("Prefix information")
        .with_description(description)
        .with_field("New prefix", prefix, true);
    
    if let Some(prefix) = old_prefix {
        send_embed.field("Old prefix", prefix, true);
    }
        
    send_embed.await?;

    Ok(())
}