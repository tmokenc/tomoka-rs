use crate::commands::prelude::*;
use requester::smogon::{MoveSet, SmogonApi as _, SmogonStrategy};
use scraper::{Html};
use magic::traits::MagicIter;
use serenity_ext::CreateEmbed;
use std::num::NonZeroUsize;

struct Strategies {
    data: Vec<SmogonStrategy>,
}

impl Paginator for Strategies {
    fn total_pages(&self) -> Option<usize> {
        let pages = self.data.iter().map(|v| v.movesets.len()).sum();
        Some(pages)
    }
    
    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed) {
        let (data, format) = {
            let page = page.get();
            let mut counter = 0;
            let mut data = None;
            let mut format: Option<&str> = None;
            'outer: for strategy in &self.data {
                for moveset in &strategy.movesets {
                    counter += 1;
                    if counter == page {
                        format = Some(strategy.format.as_ref());
                        data = Some(moveset);
                        break 'outer
                    }
                }
            }
            
            match data.zip(format) {
                Some(value) => value,
                None => {
                    embed.description("Cannot find any data with the given index");
                    return
                }
            }
        };
        
        embed.title(format!("{} strategy: {}", data.pokemon, data.name));
        embed.description(format_description(&data.description));
        embed.field("Moveset", format_moveset(&data), false);
        embed.thumbnail(format!(
            "https://www.smogon.com/dex/media/sprites/xy/{}.gif",
            data.pokemon.replace(" ", "-").to_lowercase(),
        ));
        embed.footer(|f| f.text(format!("Format: {}", format)));
    }
}

#[command]
#[aliases("smogon", "strategy")]
async fn smogon_strategy(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let pokemon = args.rest();
    let aliasized = pokemon.replace(" ", "-").to_lowercase();

    let strategies = get_data::<ReqwestClient>(&ctx)
        .await
        .unwrap()
        .strategy(&aliasized, Default::default())
        .await?;

    if strategies.is_empty() {
        Err(format!("Not found any strategy for the pokemon **{}**", pokemon).into())
    } else {
        let data = Strategies {
            data: strategies,
        };
        
        data.pagination(ctx, msg).await?;
        Ok(())
    }
}

fn format_description(d: &str) -> String {
    let fragment = Html::parse_fragment(d);
    fragment.root_element().text().collect()
}

fn format_moveset(m: &MoveSet) -> String {
    let mut result = format!(
        "{}\n**Item**: {}\n**Nature**: {}\n**Ability**: {}\n**EVs**: {}",
        m.moveslots
            .iter()
            .zip(1..)
            .map(|(v, i)| {
                let moves = v.iter().map(|v| {
                    let mut res = v.name.to_owned();
                    if let Some(ref typ) = v.typ {
                        res.push(' ');
                        res.push_str(typ);
                    }
                    res
                }).join(" / ");
                
                format!("{}. {}\n", i, moves)
                
            })
            .collect::<String>(),
        m.items.join(" / "),
        m.nature(),
        m.ability(),
        m.ev_config().join(" | "),
    );

    if !m.ivconfigs.is_empty() {
        let iv = format!("\n**__IV__**: {}", m.iv_config().join(" | "));
        result.push_str(&iv);
    }

    result
}
