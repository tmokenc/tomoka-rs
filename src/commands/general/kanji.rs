use crate::commands::prelude::*;
use requester::mazii::MaziiApi as _;

#[command]
#[aliases("k")]
#[usage = "<Kanji(s)>"]
#[example = "智花"]
/// Get the details meaning of kanji(s)
async fn kanji(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let content = args.rest().trim();
    if content.is_empty() {
        return Err("Please enter some kanji".into());
    }
    
    msg.channel_id.broadcast_typing(&ctx).await?;
    let reqwest = get_data::<ReqwestClient>(&ctx).await.unwrap();
    let result = reqwest.kanji(&content).await?;
    result.send_embed(ctx, msg.channel_id).await?;
    Ok(())
}
