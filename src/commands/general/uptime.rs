use crate::commands::prelude::*;
use crate::storages::InforKey;
use serenity_ext::ChannelExt;

use humantime::format_duration;


#[command]
/// To see how long I have been up!
async fn uptime(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let uptime = ctx
        .data
        .read()
        .await
        .get::<InforKey>()
        .unwrap()
        .uptime();
        
    msg.channel_id
        .send_embed(ctx)
        .with_title("Uptime")
        .with_description(format!("I have been up for **{}**", format_duration(uptime)))
        .with_color(crate::read_config().await.color.information)
        .with_current_timestamp()
        .await?;

    Ok(())
}
