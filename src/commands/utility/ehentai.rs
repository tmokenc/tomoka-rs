use crate::commands::prelude::*;
use requester::ehentai::EhentaiApi as _;

#[command]
#[aliases("e-h", "eh", "e-hentai", "sadpanda", "sadkaede")]
/// Get information of a E-H or ExH gallery
async fn ehentai(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let content = match msg.referenced_message {
        Some(ref msg) => msg.content.as_str(),
        None => args.rest(),
    };
    
    let data = parse_eh_token(content);

    if data.is_empty() {
        return Err("Error 404 Not found SadKaede in the content...".into());
    };

    let nsfw = is_nsfw_channel(&ctx, msg.channel_id).await;
    let data = get_data::<ReqwestClient>(&ctx)
        .await
        .unwrap()
        .gmetadata(data)
        .await?
        .into_iter()
        .filter(|v| nsfw || v.is_sfw())
        .collect::<Vec<_>>();

    if data.is_empty() {
        return Err("Succesfully Not Found".into());
    }

    data.pagination(ctx, msg).await?;

    Ok(())
}