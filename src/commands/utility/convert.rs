use crate::commands::prelude::*;
use crate::Result;
use std::collections::HashMap;
use requester::currency::{CurrencyApi as _, Currency};
use uom::{Conversion, ConstantOp, si};
use regex::Regex;
use lazy_static::lazy_static;
use num_traits::One as _;

type SIType = f64;

struct SIData {
    coefficient: SIType,
    constant_add: SIType,
    constant_sub: SIType,
    singular: &'static str,
    plural: &'static str,
}

impl<U: Conversion<SIType, T = SIType> + si::Unit + Sized> From<U> for SIData {
    fn from(_u: U) -> Self {
        Self {
            coefficient: U::coefficient(),
            constant_add: U::constant(ConstantOp::Add),
            constant_sub: U::constant(ConstantOp::Sub),
            singular: U::singular(),
            plural: U::plural(),
        }
    }
}

enum Type {
    Currency(&'static Currency, &'static Currency),
    SI(SIData, SIData),
}

macro_rules! parse_unit {
    ($name:ident, $( $x:ident ),*) => {
        fn $name(s: &str) -> Option<SIData> {
            si::$name::units().find(|v| {
                v.abbreviation() == s
                || v.singular() == s
                || v.plural() == s
            })
            .and_then(|v| match v {
                $(
                    si::$name::Units::$x(v) => Some(v.into()),
                )*
                _ => None
            })
        }
    }
}

parse_unit! { 
    length,
    angstrom, astronomical_unit, attometer, centimeter, chain, 
    decameter, decimeter, exameter, fathom, femtometer, fermi, foot, foot_survey, 
    gigameter, hectometer, inch, kilometer, light_year, megameter, 
    meter, microinch, micrometer, micron, mil, mile, mile_survey, 
    millimeter, nanometer, nautical_mile, parsec, petameter, 
    pica_computer, pica_printers, picometer, point_computer, point_printers, 
    rod, terameter, yard, yoctometer, yottameter, zeptometer, zettameter
}

parse_unit! {
    mass,
    attogram, carat, centigram, decagram, decigram, exagram, 
    femtogram, gigagram, grain, gram, hectogram, hundredweight_long, 
    hundredweight_short, kilogram, megagram, microgram, milligram, 
    nanogram, ounce, ounce_troy, pennyweight, petagram, picogram, 
    pound, pound_troy, slug, teragram, ton, ton_assay, ton_long, 
    ton_short, yoctogram, yottagram, zeptogram, zettagram
}

parse_unit! {
    information,
    bit, byte, crumb, deciban, exabit, exabyte, exbibit, exbibyte, 
    gibibit, gibibyte, gigabit, gigabyte, hartley, kibibit, kibibyte, 
    kilobit, kilobyte, mebibit, mebibyte, megabit, megabyte, 
    natural_unit_of_information, nibble, octet, pebibit, pebibyte, 
    petabit, petabyte, shannon, tebibit, tebibyte, terabit, terabyte, 
    trit, yobibit, yobibyte, yottabit, yottabyte, zebibit, zebibyte, 
    zettabit, zettabyte
}

parse_unit! {
    time,
    attosecond, centisecond, day, day_sidereal, decasecond, decisecond, 
    exasecond, femtosecond, gigasecond, hectosecond, hour, hour_sidereal, 
    kilosecond, megasecond, microsecond, millisecond, minute, nanosecond, 
    petasecond, picosecond, second, second_sidereal, shake, terasecond, 
    year, year_sidereal, year_tropical, yoctosecond, yottasecond, 
    zeptosecond, zettasecond
}

parse_unit! {
    thermodynamic_temperature,
    attokelvin, centikelvin, decakelvin, decikelvin, degree_celsius, 
    degree_fahrenheit, degree_rankine, exakelvin, femtokelvin, gigakelvin, 
    hectokelvin, kelvin, kilokelvin, megakelvin, microkelvin, millikelvin, 
    nanokelvin, petakelvin, picokelvin, terakelvin, yoctokelvin, yottakelvin, 
    zeptokelvin, zettakelvin
}

parse_unit! {
    velocity,
    attometer_per_second, centimeter_per_second, decameter_per_second, 
    decimeter_per_second, exameter_per_second, femtometer_per_second, 
    foot_per_hour, foot_per_minute, foot_per_second, gigameter_per_second, 
    hectometer_per_second, inch_per_second, kilometer_per_hour, 
    kilometer_per_second, knot, megameter_per_second, meter_per_second, 
    micrometer_per_second, mile_per_hour, mile_per_minute, 
    mile_per_second, millimeter_per_minute, millimeter_per_second, 
    nanometer_per_second, petameter_per_second, picometer_per_second, 
    terameter_per_second, yoctometer_per_second, yottameter_per_second, 
    zeptometer_per_second, zettameter_per_second
}

parse_unit! {
    volume,
    acre_foot, attoliter, barrel, bushel, centiliter, cord, cubic_attometer, 
    cubic_centimeter, cubic_decameter, cubic_decimeter, cubic_exameter, 
    cubic_femtometer, cubic_foot, cubic_gigameter, cubic_hectometer, 
    cubic_inch, cubic_kilometer, cubic_megameter, cubic_meter, cubic_micrometer, 
    cubic_mile, cubic_millimeter, cubic_nanometer, cubic_petameter, 
    cubic_picometer, cubic_terameter, cubic_yard, cubic_yoctometer, 
    cubic_yottameter, cubic_zeptometer, cubic_zettameter, cup, decaliter, 
    deciliter, exaliter, femtoliter, fluid_ounce, fluid_ounce_imperial, 
    gallon, gallon_imperial, gigaliter, gill, gill_imperial, hectoliter, 
    kiloliter, liter, megaliter, microliter, milliliter, nanoliter, peck, 
    petaliter, picoliter, pint_dry, pint_liquid, quart_dry, quart_liquid, 
    register_ton, stere, tablespoon, teaspoon, teraliter, yoctoliter, 
    yottaliter, zeptoliter, zettaliter
}

parse_unit! {
    angle,
    degree, gon, mil, minute, radian, revolution, second
}

fn currency(s: &str) -> Option<&'static Currency> {
    lazy_static! {
        static ref CURRENCIES: HashMap<String, Currency> = {
            let file = std::fs::read("./assets/data/currency.json").unwrap();
            serde_json::from_slice(&file).unwrap()
        };
    }
    
    CURRENCIES.get(&s.to_uppercase())
}

fn parse_args(args: &str) -> Result<(f64, Type)> {
    lazy_static! {
        static ref REG: Regex = Regex::new(r"(\d*)\s*?(.+)\s+?to\s+?(.+)").unwrap();
    }
    
    let captures = REG.captures(args).ok_or("Cannot read the arguments")?;
    let from = captures.get(2).ok_or("What type is it???")?.as_str().trim();
    let to = captures.get(3).ok_or("What type it is going to be")?.as_str().trim();
        
    if from.is_empty() || to.is_empty() {
        return Err("What to convert???".into())
    }
    
    let value: f64 = {
        let capture = captures.get(1).ok_or("Cannot get the value to convert")?.as_str();
            
        if capture.is_empty() { 
            1.0 
        } else { 
            capture.parse()? 
        }
    };
    
    macro_rules! parse_units {
        ($name:ident, $t:ident) => {
            if let Some((from, to)) = $name(from).and_then(|v| $name(to).map(|x| (v, x))) {
                return Ok((value, Type::$t(from, to)))
            }
        }
    }
    
    parse_units!(currency, Currency);
    parse_units!(length, SI);
    parse_units!(mass, SI);
    parse_units!(information, SI);
    parse_units!(volume, SI);
    parse_units!(time, SI);
    parse_units!(angle, SI);
    parse_units!(thermodynamic_temperature, SI);
    parse_units!(velocity, SI);
    
    Err("Cannot find any type of unit to be converted".into())
}

#[command]
async fn convert(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let (value, typ) = parse_args(args.rest())?;
    
    let response = match typ {
        Type::Currency(from, to) => {
            let apikey = crate::read_config()
                .await
                .apikeys
                .currency
                .to_owned()
                .ok_or("Cannot get the api key for currency")?;
                
            let rate: f64 = get_data::<ReqwestClient>(&ctx)
                .await
                .ok_or("Cannot get the HTTP client")?
                .currency_rate(&from.id, &to.id, &apikey)
                .await?;
            
            format!("{} {}", value * rate, to.name)
        }
        
        Type::SI(from, to) => {
            let base = value * from.coefficient + from.constant_add;
            let result = (base - to.constant_sub) / to.coefficient;
            
            let unit_name = if result.is_one() {
                to.singular
            } else {
                to.plural
            };
            
            format!("{} {}", result, unit_name)
        }
    };
    
    msg.reply(ctx, response).await?;
    Ok(())
}