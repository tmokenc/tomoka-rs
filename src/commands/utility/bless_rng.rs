use crate::commands::prelude::*;
use serenity_ext::ChannelExt;

#[command]
#[aliases("blessrng", "wish", "mygon")]
async fn bless_rng(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(id) = &msg.guild_id {
        let list = crate::read_config().await.bad_luck_people.to_owned();
        for mem in list {
            id.disconnect_member(ctx, mem).await?;
        }
    }
    
    msg.channel_id
        .send_embed(ctx)
        .with_description("ALL GREEN!!!\nWish for you good luck")
        .with_image("https://cdn.discordapp.com/attachments/450521152272728065/708817978594033804/Diancie.gif")
        .await?;
       
    Ok(())
}