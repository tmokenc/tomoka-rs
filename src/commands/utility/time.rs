use crate::commands::prelude::*;
use chrono::{Timelike, TimeZone, DateTime};
use chrono_tz::*;
use serenity_ext::events::{Event, RawEventHandlerRef};
use serenity_ext::ChannelExt;
use serenity::model::event::RelatedId;
use serenity::model::id::UserId;

use std::sync::atomic::{AtomicI64, AtomicBool, Ordering};

struct MyonTracking {
    in_voice: AtomicBool,
    last_seen: AtomicI64,
}

impl MyonTracking {
    fn slept(&self, time: &DateTime<Tz>) -> bool {
        if time.hour() > 6 {
            return true;
        }

        if self.in_voice.load(Ordering::SeqCst) {
            return false;
        }

        let last_seen = Utc.timestamp(self.last_seen.load(Ordering::SeqCst), 0);
        let duration = time.signed_duration_since(last_seen);
        duration.num_minutes() > 5
    }
}

static MYON: MyonTracking = MyonTracking {
    in_voice: AtomicBool::new(false),
    last_seen: AtomicI64::new(0),
};

pub struct MyonTimeTracker;

#[async_trait::async_trait]
impl RawEventHandlerRef for MyonTimeTracker {
    async fn raw_event_ref(&self, _ctx: &Context, ev: &Event) {
        match ev.user_id() {
            RelatedId::Some(UserId(353026384601284609)) => {
                MYON.last_seen.store(Utc::now().timestamp(), Ordering::SeqCst);
            }
            _ => return,
        };

        match ev {
            Event::VoiceStateUpdate(e) => {
                let voice_channel = e
                    .voice_state
                    .channel_id
                    .is_some();

                MYON.in_voice.store(voice_channel, Ordering::SeqCst);
            }

            _ => {}
        }
    }

}

fn myon_time_formatted(time: &DateTime<Tz>) -> String {
    let hcm_time = time.with_timezone(&Asia::Ho_Chi_Minh);

    if MYON.slept(&hcm_time) {
        return hcm_time.format("%H:%M %d/%m/%Y").to_string()
    }

    log::info!("Display myon time");

    let myon_time = hcm_time - chrono::Duration::days(1);

    format!("{}:{}", myon_time.hour() + 24, myon_time.format("%M %d/%m/%Y"))
}

#[command]
/// Get time for various timezone
/// Passing a timestamp (from 01/01/1970 in second) to get time for a specific time
/// Will support other time parsing soon:tm:
async fn time(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let time = args
        .find::<i64>()
        .unwrap_or_else(|_| msg.timestamp.timestamp());

    let utc = UTC.timestamp(time, 0);
    let times = vec![
        ("Pacific", utc.with_timezone(&US::Pacific)),
        ("UTC", utc),
        ("CET", utc.with_timezone(&CET)),
        ("Vietnam", utc.with_timezone(&Asia::Ho_Chi_Minh)),
        ("Japan", utc.with_timezone(&Japan)),
    ];

    let config = crate::read_config().await;
    let format = config.time.format.to_owned();
    drop(config);

    msg.channel_id
        .reply_embed(ctx, msg)
        .with_timestamp(msg.timestamp.to_rfc3339())
        .with_description(format!("Current time: <t:{}>", utc.timestamp()))
        .with_fields(times.iter().map(|(name, time)| {
            let tz = &time.format("%:z").to_string()[..3];
            let embed_name = format!("{} (GMT{})", name, tz);
            let embed_value = time.format(&format);

            (embed_name, embed_value.to_string(), false)
        }))
        .with_field("Myon (GMT++)", myon_time_formatted(&utc), false)
        .await?;

    Ok(())
}
