use crate::commands::prelude::*;
use requester::r18::*;
use crate::utils;

#[command]
pub async fn jav(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    if !utils::is_nsfw_channel(ctx, msg.channel_id).await {
        return Ok(())
    }
    
    utils::typing(ctx, msg.channel_id);
    
    let req = utils::get_data::<ReqwestClient>(ctx).await.ok_or("Cannot get the http client")?;
    let query = args.rest().trim().to_uppercase();
    let search_data = req.r18search(&query, 1).await?;
    
    if search_data.contents.is_empty() {
        return Err("Cannot find and title that is match with the query".into());
    }
    
    if search_data.contents.len() == 1 {
        let data = req.r18content(&search_data.contents[0].id).await?;
        data.send_embed(ctx, msg.channel_id).await?;
        return Ok(())
    }
    
    let matches = search_data
        .contents
        .iter()
        .find(|v| v.label.to_uppercase() == query);
        
    if let Some(content) = matches {
        let data = req.r18content(&content.id).await?;
        data.send_embed(ctx, msg.channel_id).await?;
    } else {
        search_data.pagination(ctx, msg).await?;
    }
    
    Ok(())
}
