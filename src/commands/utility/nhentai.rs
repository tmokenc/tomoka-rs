use crate::commands::prelude::*;
use serenity::model::channel::ReactionType;
use std::time::Duration;
use requester::NhentaiScraper as _;
use serenity_ext::ChannelExt;
use serenity::builder::{CreateComponents, CreateActionRow, CreateButton};
use serenity::model::interactions::message_component::ButtonStyle;

fn get_id(ctx: &Context, msg: &Message, mut args: Args) -> Option<u64> {
    match msg.referenced_message {
        Some(ref msg) => msg
            .content
            .split_whitespace()
            .find_map(|v| v.parse::<u64>().ok()),
        None => args.find::<u64>().ok(),
    }
}

#[command]
#[aliases("nhen")]
async fn nhentai(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    if !is_nsfw_channel(ctx, msg.channel_id).await {
        return Ok(())
    }
    
    let id = get_id(ctx, msg, args);
    let gallery = get_data::<ReqwestClient>(ctx)
        .await
        .ok_or(magic::Error)?
        .gallery(id)
        .await?
        .ok_or(format!("Cannot find any with the magic number {}", id.unwrap_or(0)))?;
    
    let emoji = ReactionType::Unicode(String::from("📖"));
    let mut button = CreateButton::default();
    button.label("Read");
    button.emoji(emoji);
    button.custom_id("nhen_read");
    button.style(ButtonStyle::Secondary);
    
    let mut row = CreateActionRow::default();
    row.add_button(button);
    
    let mut components = CreateComponents::default();
    components.add_action_row(row);

    let sent = msg.channel_id
        .send_embed(ctx)
        .with_embed(gallery.embed_data())
        .with_components(components)
        .await?;
    
    let collector = sent
        .await_component_interaction(&ctx)
        .timeout(Duration::from_secs(30))
        .author_id(msg.author.id)
        .await;
    
    serenity_ext::remove_components(ctx, sent.channel_id, sent.id).await?;
        
    if let Some(_interaction) = collector {
        gallery.pagination(ctx, msg).await?;
    }
    
    Ok(())
}
