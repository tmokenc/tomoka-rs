use crate::commands::prelude::*;
use rand::prelude::*;

#[command]
#[aliases("rand", "rng")]
async fn random(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let mut rng = StdRng::from_entropy();
    let args = args
        .rest()
        .split(",")
        .map(|v| v.trim())
        .filter(|v| !v.is_empty())
        .collect::<Vec<_>>();
    
    if let Some(arg) = args.choose(&mut rng) {
        msg.reply(ctx, rand_with_arg(arg, &mut rng)?).await?;
    } else {
        msg.reply(ctx, rng.gen::<f64>()).await?;
    };
    
    Ok(())
}

fn rand_with_arg(arg: &str, rng: &mut StdRng) -> crate::Result<String> {
    log::info!("rand pattern is: {}", &arg);
    let mut iter = arg.chars();
    let mut s = String::new();
        
    let mut num_tmp: Option<u16> = None;
    
    while let Some(ch) = iter.next() {
        match ch {
            '0'..='9' => {
                let num = ch.to_digit(10).unwrap();
                let mut n = num_tmp.take().unwrap_or(0) * 10 + num as u16;
                
                if n == 0 {
                    continue 
                }
                
                if n > 2000 {
                    n = 2000;
                }
                
                num_tmp = Some(n);
            }
            
            'd' => {
                let iter = std::iter::repeat(())
                    .map(|_| rng.gen_range(0..10))
                    .filter_map(|n| char::from_digit(n, 10))
                    .take(num_tmp.take().unwrap_or(1) as _);
                    
                for c in iter {
                    s.push(c);
                    if s.len() > 2000 {
                        return Ok(s)
                    }
                }
            }
            
            'h' | 'H' => {
                let iter = std::iter::repeat(())
                    .map(|_| rng.gen_range(0..16))
                    .filter_map(|n| char::from_digit(n, 16))
                    .take(num_tmp.take().unwrap_or(1) as _);
                    
                for mut c in iter {
                    if ch.is_ascii_uppercase() {
                        c.make_ascii_uppercase();
                    }
                    
                    s.push(c);
                    if s.len() > 2000 {
                        return Ok(s)
                    }
                } 
            }
            
            '~' => {
                return Err("Not implemented yet".into())
            }
            
            v => s.push(v),
        }
    }
        
    
    Ok(s)
}