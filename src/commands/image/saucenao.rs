use super::get_last_image_url;
use crate::commands::prelude::*;
use requester::SauceNaoScraper as _;

#[command]
#[aliases("sauce")]
/// Find an anime image source.
async fn saucenao(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let depth = crate::read_config().await.image.search_depth;
    let img = match get_last_image_url(&ctx, &msg, depth).await {
        Some(i) => i,
        None => {
            return Err(format!("Cannot find an image from last {} message", depth).into());
        }
    };

    let similarity = args.raw().find_map(|v| {
        if v.ends_with('%') {
            v[..v.len() - 1].parse::<f32>().ok()
        } else {
            None
        }
    });

    let data = get_data::<ReqwestClient>(&ctx)
        .await
        .unwrap()
        .saucenao(&img, similarity)
        .await?;

    if data.not_found() {
        return Err("Error 404: Sauce Not Found".into());
    }
    
    data.send_embed(ctx, msg.channel_id).await?;

    Ok(())
}
