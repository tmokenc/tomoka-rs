use crate::commands::prelude::*;
use qrcode::QrCode;
use image::{load_from_memory, DynamicImage, Rgb, ImageOutputFormat};
use serenity_ext::ChannelExt;

#[command]
#[aliases("qr", "qrcode")]
/// Generate/Read a QrCode
/// if has image, decode it
/// if given text, generate QR code from it
/// if both present, decode the image and ignore the text
/// if none, try to find an image from the last 20 message and decode it
async fn qr_code(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let text = args.rest();
    if text.is_empty() {
        let depth = crate::read_config().await.image.search_depth;
        if let Some(buf) = super::get_last_image_buf(ctx, msg, depth).await {
            let image = load_from_memory(buf.as_ref())?;
            let img_gray = image.into_luma8();
            let mut decoder = quircs::Quirc::default();
            let codes = decoder.identify(img_gray.width() as usize, img_gray.height() as usize, &img_gray);
            let mut result = String::new();

            for code in codes.filter_map(|v| v.ok()) {
                let data = code.decode()?;
                if let Ok(s) = std::str::from_utf8(data.payload.as_ref()) {
                    result.push_str(s);
                    result.push('\n');
                }
            }

            msg.reply(ctx, result).await?;
            return Ok(());
        }
        return Err("Huh????".into());
    }

    let qr = QrCode::new(text)?;
    let mut image = DynamicImage::ImageRgb8(qr.render::<Rgb<u8>>().build());
    let mut buffer = Vec::new();

    super::watermark(&mut image).await?;

    image.write_to(&mut buffer, ImageOutputFormat::Jpeg(80))?;
    msg.channel_id.send_file(ctx, (buffer.as_slice(), "tomo_qrcode.jpg")).await?;
    Ok(())
}
