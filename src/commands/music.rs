use serenity::framework::standard::macros::group;
use serenity_ext::{Embedable, Paginator};
use crate::commands::prelude::*;

#[group]
#[commands(play, join, leave, now_playing, queue, remove, pause, stop, skip, previous, looping, shuffle, volume)]
#[only_in(guilds)]
struct Music;

#[command]
#[only_in(guilds)]
async fn join(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild_id.ok_or("Only in guild")?;

    if let Some(channel) = get_user_voice_channel(ctx, guild, msg.author.id).await {
        get_music_manager(&ctx).await.join(guild, channel, msg.channel_id).await?;
    }
    
    Err("Not in a voice channel".into())
}

#[command]
#[only_in(guilds)]
async fn leave(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild_id.ok_or("Only in guild")?;
    get_music_manager(&ctx).await.leave(guild).await?;
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn play(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let guild_id = msg.guild_id.ok_or("Only playable in guild")?;
    let song = args.rest();
    
    let manager = get_music_manager(&ctx).await;
    manager.play(guild_id, msg.channel_id, song, &msg.author).await?;
    
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn remove(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let guild_id = msg.guild_id.ok_or("Only playable in guild")?;
    let idx = args.single::<u8>().map_err(|_| "Need to provide an index to the song you wish to be removed")?;
    
    let manager = get_music_manager(&ctx).await;
    manager.remove(guild_id, idx).await;
    
    Ok(())
}

#[command]
#[aliases("np")]
#[only_in(guilds)]
async fn now_playing(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        let player = get_music_manager(&ctx)
            .await
            .get_player(guild)
            .await
            .ok_or("Who am I? Where am I?")?;
        
        let embed = player
            .current()
            .map(|s| s.embed_data())
            .ok_or("Currently no song is being played yet...")?;
            
        drop(player);
        
        embed.send_embed(&ctx, msg.channel_id).await?;
    }
    
    Ok(())
}

#[command]
#[aliases("list", "songs")]
#[only_in(guilds)]
async fn queue(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        if let Some(player) = get_music_manager(&ctx).await.get(guild).await {
            player.pagination(ctx, msg).await?;
        }
        
    }
    
    Ok(())
}

#[command]
#[aliases("prev")]
#[only_in(guilds)]
async fn previous(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.previous(guild).await;
    }
    
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn pause(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.pause(guild).await;
    }
    
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn stop(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.stop(guild).await;
    }
    
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn skip(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.skip(guild).await;
    }
    
    Ok(())
}

#[command("loop")]
#[only_in(guilds)]
async fn looping(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.looping(guild).await;
    }
    
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn shuffle(ctx: &Context, msg: &Message) -> CommandResult {
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.shuffle(guild).await;
    }
    
    Ok(())
}

#[command]
#[only_in(guilds)]
async fn volume(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let mut volume = args.rest().trim().parse::<u8>()?;
    
    if volume > 100 {
        volume = 100;
    }
    
    if let Some(guild) = msg.guild_id {
        get_music_manager(&ctx).await.volume(guild, volume as f32 / 100.0).await;
    }
    
    Ok(())
}

async fn get_music_manager(ctx: &Context) -> <MusicKey as TypeMapKey>::Value {
    get_data::<MusicKey>(ctx).await.expect("The music manager")
}
