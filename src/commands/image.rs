use crate::utils::get_file_bytes;
use bytes::Bytes;
use magic::import_all;
use serenity::client::Context;
use serenity::framework::standard::macros::group;
use serenity::model::channel::Message;
use crate::commands::prelude::*;
use serenity_ext::ChannelExt as _;
use tokio::task;
use image::*;

import_all! {
    saucenao,
    qr_code
}

#[group]
#[commands(gif, rotate, flip, grayscale, saucenao, diancie, qr_code, jail, jail_rgb)]
struct Image;

#[command]
/// Show an extremely cute Diancie (pokemon)
async fn diancie(ctx: &Context, msg: &Message, _: Args) -> CommandResult {
    msg.channel_id.send_file(ctx, "./assets/img/Diancie.gif").await?;
    Ok(())
}

#[command]
/// Rgb in jail
async fn jail(ctx: &Context, msg: &Message, _: Args) -> CommandResult {
    msg.channel_id
        .send_embed(ctx)
        .with_image("https://cdn.discordapp.com/attachments/512404979772948489/549829129906814976/RGB_in_jail.png")
        .await?;

    Ok(())
}

#[command]
#[aliases("jailrgb")]
/// Rgb in jail animated
async fn jail_rgb(ctx: &Context, msg: &Message, _: Args) -> CommandResult {
    msg.channel_id
        .send_embed(ctx)
        .with_image("https://cdn.discordapp.com/attachments/512404979772948489/549828250118127628/RGB-in-jail1.gif")
        .await?;

    Ok(())
}

#[command]
/// Get a random gif image on Tenor with the given query
async fn gif(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    use requester::tenor::{
        TenorSearchOption,
        ContentFilter,
        TenorApi as _
    };

    let key = crate::read_config()
        .await
        .apikeys
        .tenor
        .to_owned()
        .ok_or("Cannot get the Tenor API key")?;

    let filter = if is_nsfw_channel(ctx, msg.channel_id).await {
        ContentFilter::Off
    } else {
        ContentFilter::Medium
    };

    let opt = TenorSearchOption {
        query: args.rest(),
        key: key.as_str(),
        contentfilter: filter,
        limit: 1,
        ..Default::default()
    };

    let data = get_data::<ReqwestClient>(ctx)
        .await
        .ok_or("Cannot get HTTP client")?
        .random(opt)
        .await?;

    let img = data
        .results
        .get(0)
        .ok_or("Cannot get gif with the given query")?;

    let ref media = img.media.get(0).ok_or("Nope")?;
    let ref gif = media.mediumgif;

    msg.channel_id.send_embed(ctx).with_image(&gif.url).await?;

    Ok(())
}

#[command]
/// Flip the last image from last 20 messages on the channel
async fn flip(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let depth = crate::read_config().await.image.search_depth;
    let buf = get_last_image_buf(&ctx, &msg, depth)
        .await
        .ok_or("Coudn't find an image in the most recent messages")?;

    let arg = args.single::<String>().unwrap_or_default();
    let direction = ImageProcess::Flip(match arg.to_lowercase().as_str() {
        "left" | "right" | "vertical" | "vertically" | "v" => FlipType::Vertical,
        _ => FlipType::Horizontal,
    });

    let data = image_process_async(buf, direction).await?;
    msg.channel_id.send_file(ctx, (data.as_slice(), "res.png")).await?;
    Ok(())
}

#[command]
/// Rotate the last image from 20 most recent message on the channel
async fn rotate(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let depth = crate::read_config().await.image.search_depth;
    let image_buf = get_last_image_buf(&ctx, &msg, depth)
        .await
        .ok_or("Coudn't find an image in the most recent messages")?;

    let arg = args.single::<String>().unwrap_or_default();
    let angle = ImageProcess::Rotate(match arg.to_lowercase().as_ref() {
        "90" | "90%" | "90°" | "left" => RotateAngle::Left,
        "180" | "180%" | "180°" | "rotate180" => RotateAngle::Rotate180,
        _ => RotateAngle::Right,
    });

    let image = image_process_async(image_buf, angle).await?;
    msg.channel_id.send_file(ctx, (image.as_slice(), "res.png")).await?;
    Ok(())
}

#[command]
async fn grayscale(ctx: &Context, msg: &Message) -> CommandResult {
    let depth = crate::read_config().await.image.search_depth;
    let image_buf = get_last_image_buf(&ctx, &msg, depth)
        .await
        .ok_or("Coudn't find an image in the most recent messages")?;

    let image = image_process_async(image_buf, ImageProcess::GrayScale).await?;
    msg.channel_id.send_file(ctx, (image.as_slice(), "res.png")).await?;
    Ok(())
}

/// Get the last image buf from most recent message on the channel
/// Max messages length is 100
pub async fn get_last_image_buf(ctx: &Context, msg: &Message, limit: u16) -> Option<Bytes> {
    let url = get_last_image_url(ctx, msg, limit).await?;
    get_file_bytes(url).await.ok()
}

pub async fn get_last_image_url(ctx: &Context, msg: &Message, limit: u16) -> Option<String> {
    if let Some(ref ref_msg) = msg.referenced_message {
        return get_image_url_from_message(&*ref_msg)
    }

    match get_image_url_from_message(&msg) {
        None => msg.channel_id
            .messages(ctx, |m| m.limit(limit as u64).before(msg.id))
            .await
            .ok()?
            .into_iter()
            .find_map(|v| get_image_url_from_message(&v)),
        v => v,
    }
}

#[inline]
fn get_image_url_from_message(msg: &Message) -> Option<String> {
    msg.attachments
        .iter()
        .find(|v| v.width.is_some())
        .map(|v| v.url.to_owned())
        .or_else(|| {
            msg.embeds
                .iter()
                .find_map(|v| v.image.as_ref())
                .map(|v| v.url.to_owned())
        })
}


pub enum ImageProcess {
    Rotate(RotateAngle),
    Flip(FlipType),
    GrayScale,
}

pub enum RotateAngle {
    Left,
    Right,
    Rotate180,
}

pub enum FlipType {
    Vertical,
    Horizontal,
}

#[inline]
async fn image_process_async(buf: Bytes, process: ImageProcess) -> crate::Result<Vec<u8>> {
    task::spawn_blocking(move || image_process(buf.as_ref(), process)).await?
}

/// Process an image
fn image_process(buf: &[u8], process: ImageProcess) -> crate::Result<Vec<u8>> {
    let (image, format) = get_image(buf)?;
    let result = match process {
        ImageProcess::Rotate(direction) => match direction {
            RotateAngle::Left => image.rotate270(),
            RotateAngle::Right => image.rotate90(),
            RotateAngle::Rotate180 => image.rotate180(),
        },

        ImageProcess::Flip(direction) => match direction {
            FlipType::Horizontal => image.fliph(),
            FlipType::Vertical => image.flipv(),
        },

        ImageProcess::GrayScale => image.grayscale(),
    };

    let mut data = Vec::with_capacity(buf.as_ref().len());
    let out_format = get_output_format(format);
    result.write_to(&mut data, out_format)?;

    Ok(data)
}

fn get_image(buf: &[u8]) -> crate::Result<(DynamicImage, ImageFormat)> {
    let format = image::guess_format(buf.as_ref())?;
    let image = image::load_from_memory(buf.as_ref())?;
    Ok((image, format))
}

// get the output format, fallback to .BMP if not supported
fn get_output_format(format: ImageFormat) -> ImageOutputFormat {
    let output = ImageOutputFormat::from(format);

    match output {
        ImageOutputFormat::Unsupported(_) => ImageOutputFormat::Bmp,
        v => v,
    }
}

pub async fn watermark(img: &mut image::DynamicImage) -> crate::Result<()> {
    let path = crate::read_config().await.image.watermark.to_owned();
    let mut mark = task::spawn_blocking(|| image::open(path)).await??;
    let (mut mwidth, mut mheight) = mark.dimensions();
    let (w, h) = img.dimensions();
    let (min_width, min_height) = (w / 8, h / 8);

    if (min_width < mwidth) || (min_height < mheight) {
        mark = mark.resize(min_width, min_height, imageops::FilterType::Triangle);
        mwidth = min_width;
        mheight = min_height;
    }

    let x = w / 2 - (mwidth / 2);
    let y = h / 2 - (mheight / 2);

    Ok(imageops::overlay(img, &mark, x, y))
}