use crate::commands::prelude::*;
use magic::{pub_import_all, import_all};
use serenity_ext::ChannelExt;
use serenity::framework::standard::macros::group;

pub_import_all! {
    time,
}

import_all! {
    convert,
    ehentai,
    nhentai,
    search,
    search_image,
    reminder,
    translate,
    corona,
    random,
    bless_rng,
    jav,
}

#[group]
#[commands(random, convert, base64, hex, jwt, search, search_image, time, ehentai, nhentai, corona, translate, bless_rng, jav)]
#[sub_groups(reminder)]
struct Utility;

#[command]
#[min_args(1)]
/// if the given string is a hex string, decode it, otherwise encode it
async fn hex(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let s = get_content(msg, &args);
    let hex = s.split_whitespace().collect::<String>();

    let data = match hex::decode(hex) {
        Ok(s) => String::from_utf8(s)?,
        Err(_) => hex::encode(s)
    };

    msg.reply(ctx, data).await?;

    Ok(())
}

#[command]
#[min_args(1)]
/// if the given string is a base64 string, decode it, otherwise encode it
async fn base64(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let s = get_content(msg, &args);

    let data = match base64::decode(s) {
        Ok(s) => String::from_utf8(s)?,
        Err(_) => base64::encode(s)
    };

    msg.reply(ctx, data).await?;

    Ok(())
}

#[command]
#[min_args(1)]
/// Decode the jsonwebtoken
/// ONLY DECODING FOR NOW
async fn jwt(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    use serde_json::{to_string_pretty, Value};
    use jwt::token::Unverified;

    let s = get_content(msg, &args);
    let decode = jwt::Token::<Value, Value, Unverified<'_>>::parse_unverified(s)?;

    let header = to_string_pretty(decode.header())?;
    let payload = to_string_pretty(decode.claims())?;

    msg.channel_id
        .reply_embed(ctx, msg)
        .with_field("Header", format!("```json\n{}```", header), false)
        .with_field("Payload", format!("```json\n{}```", payload), false)
        .await?;

    Ok(())
}

fn get_content<'a>(msg: &'a Message, args: &'a Args) -> &'a str {
    match msg.referenced_message {
        Some(ref m) => m.content.as_str(),
        None => args.rest(),
    }
}