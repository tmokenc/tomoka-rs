#![allow(clippy::unreadable_literal)]

#[macro_use]
extern crate log;

extern crate async_fs as fs;
extern crate config as lib_config;

mod cache;
mod commands;
mod config;
mod constants;
mod events;
mod framework;
mod logger;
mod music;
mod storages;
mod types;
mod utils;

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Result<T> = std::result::Result<T, Error>;

pub use requester::*;
pub use serenity::framework::standard::macros::hook;

use std::sync::Arc;

use crate::config::Config;
use crate::logger::EventLogger;
use cache::DbCache;
use db::DbInstance;
use events::Handler;
use storages::*;
use types::*;

use eliza::Eliza;
// use magic::dark_magic::has_external_command;
use reqwest::Client as Reqwest;
use serenity::client::bridge::gateway::{GatewayIntents, ShardManager};
use serenity::client::ClientBuilder;
use serenity::http::client::Http;
use serenity::model::id::GuildId;
use songbird::serenity::SerenityInit as _;
use tokio::runtime::Handle as TokioHandle;
use tokio::sync::{Mutex, RwLock};
use tokio::task::JoinHandle;

pub type Shard = Arc<serenity::prelude::Mutex<ShardManager>>;
pub struct Instance {
    // db: DbInstance,
    rt: TokioHandle,
    task: Option<JoinHandle<serenity::Result<()>>>,
    shard: Option<Shard>,
}

impl Drop for Instance {
    fn drop(&mut self) {
        if let Some(shard) = self.shard.take() {
            self.rt
                .spawn(async move { shard.lock().await.shutdown_all().await });
        }
    }
}

impl Instance {
    pub async fn start(token: &str, application_id: u64, db: DbInstance) -> Result<Self> {
        let token = if token.trim().starts_with("BOT ") {
            token.trim().to_owned()
        } else {
            format!("Bot {}", token.trim())
        };

        let req = Arc::new(Reqwest::new());
        let mut http = Http::new(Arc::clone(&req), &token);
        http.application_id = application_id;

        let db_clone = db.clone();
        let req_clone = Arc::clone(&req);
        tokio::spawn(async move {
            let db = db_clone;
            let req = req_clone;

            if let Err(why) = fetch_guild_config_from_db(&db).await {
                error!("Cannot fetch guilds database:\n{:#?}", why);
            }

            if let Err(why) = commands::pokemon::update_pokemon(&db, &*req).await {
                error!("\n{}", why);
            }
        });

        let rt = TokioHandle::try_current()?;
        let handler = Handler::new();
        let raw_handler = serenity_ext::MultiRawHandler::new();
        let raw_handler_clone = raw_handler.clone();
        let framework = framework::get_framework();

        raw_handler.add("Logger", EventLogger::new()).await;
        raw_handler
            .add("Myon Time", commands::utility::time::MyonTimeTracker)
            .await;

        let config = read_config().await;

        let cache = DbCache::new(db.clone())?;
        let eliza = Eliza::from_file(&config.eliza_brain).unwrap();

        let mut client = ClientBuilder::new_with_http(http)
            .framework(framework)
            .event_handler(handler)
            .raw_event_handler(raw_handler_clone)
            .intents(intents())
            .register_songbird()
            .type_map_insert::<RawEventList>(raw_handler)
            .type_map_insert::<DatabaseKey>(db.clone())
            .type_map_insert::<ReqwestClient>(Arc::clone(&req))
            .type_map_insert::<CacheStorage>(Arc::new(cache))
            .type_map_insert::<AIStore>(Arc::new(Mutex::new(eliza)))
            .await?;

        {
            let mut data = client.data.write().await;
            let info = Information::init(&client.cache_and_http.http).await?;

            data.insert::<InforKey>(info);

            // if has_external_command("ffmpeg") {
            //     data.insert::<MusicManager>(mutex_data(HashMap::new()));
            // }
        }

        let shard_manager = Arc::clone(&client.shard_manager);
        let task = rt.spawn(async move { client.start().await });

        Ok(Self {
            // db: db,
            shard: Some(shard_manager),
            task: Some(task),
            rt,
        })
    }

    /// Take out the shard
    #[inline]
    pub fn shard(&mut self) -> Option<Shard> {
        self.shard.take()
    }

    pub async fn wait(mut self) -> Result<()> {
        if let Some(task) = self.task.take() {
            task.await??;
        }

        Ok(())
    }
}

lazy_static::lazy_static! {
    pub static ref CONFIG: RwLock<Config> = RwLock::new(Config::init().unwrap());
}

#[inline]
pub async fn read_config() -> tokio::sync::RwLockReadGuard<'static, Config> {
    CONFIG.read().await
}

#[inline]
pub async fn write_config() -> tokio::sync::RwLockWriteGuard<'static, Config> {
    CONFIG.write().await
}

async fn fetch_guild_config_from_db(db: &DbInstance) -> Result<()> {
    let guilds_config = &crate::read_config().await.guilds;

    db.open_json("GuildConfig")?
        .get_all::<u64, GuildConfig>()
        .for_each(|(k, v)| {
            guilds_config.insert(GuildId(k), v);
        });

    Ok(())
}

#[inline]
fn intents() -> GatewayIntents {
    GatewayIntents::all()
        & !GatewayIntents::GUILD_BANS
        & !GatewayIntents::GUILD_EMOJIS
        & !GatewayIntents::GUILD_INTEGRATIONS
        & !GatewayIntents::GUILD_WEBHOOKS
        & !GatewayIntents::GUILD_INVITES
        & !GatewayIntents::GUILD_PRESENCES
        & !GatewayIntents::GUILD_MESSAGE_TYPING
        & !GatewayIntents::DIRECT_MESSAGE_TYPING
}
