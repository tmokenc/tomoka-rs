use super::song::TrackState;
use super::{get_music_db, Song};
use crate::utils::*;
use crate::Result;
use chrono::prelude::*;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use serenity::builder::{CreateActionRow, CreateButton, CreateComponents, CreateSelectMenu};
use serenity::model::channel::ReactionType;
use serenity::model::id::{ChannelId, GuildId, MessageId};
use serenity::model::interactions::message_component::ButtonStyle;
use serenity::prelude::Context;
use serenity_ext::{ChannelExt, CreateEmbed, Embedable, Paginator};
use songbird::Call;
use std::collections::VecDeque;
use std::fmt::{self, Write};
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::sync::Mutex;

const MAX_LIST: u8 = 50;

#[derive(Clone, Copy)]
pub enum LoopState {
    Infinitive,
    OneSong,
    None,
}

#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct ListId(u64);

impl ListId {
    fn new() -> Self {
        // nano second at 13/05/2021, when this function was born
        const MODIFY: u64 = 1620864000000000000;
        Self(chrono::Utc::now().timestamp_nanos() as u64 - MODIFY)
    }
}

#[derive(Serialize, Deserialize)]
pub struct ListDbStore {
    songs: Vec<DbSong>,
    pub(super) last_access: DateTime<Utc>,
}

impl From<&MusicPlayer> for ListDbStore {
    fn from(music: &MusicPlayer) -> Self {
        let mut songs = music
            .list
            .iter()
            .map(|song| {
                (
                    song.index,
                    DbSong {
                        url: song.url().to_owned(),
                        name: song.name().to_owned(),
                        duration: song.duration(),
                    },
                )
            })
            .collect::<Vec<_>>();

        songs.sort_by_key(|(idx, _)| *idx);

        Self {
            songs: songs.into_iter().map(|(_, song)| song).collect(),
            last_access: Utc::now(),
        }
    }
}

#[derive(Serialize, Deserialize)]
struct DbSong {
    url: String,
    name: String,
    duration: Duration,
}

impl fmt::Display for ListId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "--{:X}", self.0)
    }
}

pub struct MusicConfig {
    pub(super) loop_state: LoopState,
    pub(super) shuffle: bool,
    pub(super) volume: f32,
}

pub struct Connection {
    pub(super) ctx: Arc<Context>,
    pub(super) voice: Arc<Mutex<Call>>,
    pub(super) log_channel: ChannelId,
    pub(super) log_message: MessageId,
    pub(super) guild: GuildId,
}

impl Connection {
    pub fn send_message(&self, message: impl Embedable) {
        let ctx = Arc::clone(&self.ctx);
        let channel = self.log_channel;
        let data = message.embed_data();

        tokio::spawn(async move {
            data.send_embed(&*ctx, channel).await.ok();
        });
    }
}

impl Default for MusicConfig {
    fn default() -> Self {
        Self {
            loop_state: LoopState::None,
            shuffle: false,
            volume: 0.5,
        }
    }
}

pub(super) enum PlayState {
    Playing(u8),
    Stopped(Instant),
    Paused(u8, Instant),
}

impl PlayState {
    fn stop(&mut self) {
        if !matches!(self, Self::Stopped(_)) {
            *self = Self::Stopped(Instant::now());
        }
    }
}

pub struct MusicPlayer {
    id: ListId,
    pub(super) config: MusicConfig,
    pub(super) channel: ChannelId,
    pub(super) connection: Connection,

    inc_index: u8,
    pub(super) state: PlayState,
    list: VecDeque<Song>,
    played: u8,
}

impl Drop for MusicPlayer {
    fn drop(&mut self) {
        if let Ok(rt) = tokio::runtime::Handle::try_current() {
            let ctx = Arc::clone(&self.connection.ctx);
            let voice = Arc::clone(&self.connection.voice);
            let channel = self.connection.log_channel;
            let message = self.connection.log_message;
            let voice_channel = self.channel;
            let list_len = self.list.len();
            let id = self.id;

            rt.spawn(async move {
                if let Some(connection) = voice.lock().await.current_connection() {
                    let ctx = Arc::clone(&ctx);
                    let guild = connection.guild_id;

                    tokio::spawn(async move {
                        if let Some(bird) = songbird::get(&*ctx).await {
                            bird.remove(guild).await.ok();
                        }
                    });
                }

                let http = Arc::clone(&ctx);
                let chann = channel.0;

                let mut embed = CreateEmbed::default();
                embed.title("Music Player");
                embed.description(format!(
                    "Disconnected from the <#{}>\n Here was {} songs in the list\nYou can replay this list by using the `play` command with the ID below",
                    voice_channel,
                    list_len,
                ));
                embed.timestamp(&chrono::Utc::now());
                embed.field("List ID", id, false);
                embed.color(0x453b26);

                let embed_clone = embed.clone();
                let empty_component = CreateComponents::default();

                tokio::spawn(async move {
                    chann
                        .edit_message(&*http, message)
                        .with_embed(embed)
                        .with_components(empty_component)
                        .await
                        .ok();
                });

                tokio::spawn(async move {
                    channel
                        .send_embed(&*ctx)
                        .with_embed(embed_clone)
                        .with_title("Voice")
                        .await
                        .ok();
                })
            });
        }
    }
}

const BUTTONS: &[(&str, &str)] = &[
    ("\u{23ef}", "Play/Pause"),
    ("\u{23f9}", "Stop"),
    ("\u{23ee}", "Previous"),
    ("\u{23ed}", "Next"),
    ("\u{1f500}", "Shuffle"),
    ("\u{1f501}", "Loop"),
    ("\u{2795}", "Volume Up"),
    ("\u{2796}", "Volume Down"),
];

impl MusicPlayer {
    pub async fn new(
        ctx: Arc<Context>,
        voice: Arc<Mutex<Call>>,
        channel: ChannelId,
        log_channel: ChannelId,
    ) -> crate::Result<Self> {
        let guild = voice
            .lock()
            .await
            .current_connection()
            .ok_or("Cannot get the connection info")?
            .guild_id;

        let mut res = Self {
            id: ListId::new(),
            state: PlayState::Stopped(Instant::now()),
            config: Default::default(),
            connection: Connection {
                ctx,
                voice,
                log_channel,
                log_message: Default::default(),
                guild: GuildId(guild.0),
            },
            channel,
            list: Default::default(),
            played: 0,
            inc_index: 1,
        };

        let embed = res.embed_data();
        let buttons = res.components();
        let message = log_channel
            .0
            .send_embed(&*res.connection.ctx)
            .with_embed(embed)
            .with_components(buttons)
            .await?;

        res.connection.log_message = message.id;

        Ok(res)
    }

    fn select_menu(&self) -> CreateSelectMenu {
        let mut menu = CreateSelectMenu::default();

        let opts = self.list.iter().map(|v| v.menu_option()).collect::<Vec<_>>();

        menu.custom_id("SongSelect");
        menu.placeholder("Select a song to jump to");
        menu.options(|f|  f.set_options(opts) );

        menu
    }

    pub fn components(&self) -> CreateComponents {
        let mut component = CreateComponents::default();
        let mut row1 = CreateActionRow::default();
        let mut row2 = CreateActionRow::default();
        let mut row3 = CreateActionRow::default();

        row3.add_select_menu(self.select_menu());

        BUTTONS
            .iter()
            .map(|(emo, text)| {
                let mut button = CreateButton::default();
                button.label(text);
                button.emoji(ReactionType::Unicode(String::from(*emo)));
                button.style(ButtonStyle::Primary);
                button.custom_id(text);
                button
            })
            .enumerate()
            .for_each(|(i, button)| {
                if i >= 4 {
                    row2.add_button(button);
                } else {
                    row1.add_button(button);
                }
            });

        component.add_action_row(row1);
        component.add_action_row(row2);
        component.add_action_row(row3);
        component
    }

    #[inline]
    pub fn update_message(&self) {
        let data = self.embed_data();
        let ctx = Arc::clone(&self.connection.ctx);
        let message = self.connection.log_message;
        let channel = self.connection.log_channel;
        let buttons = self.components();

        tokio::spawn(async move {
            let edit_message = channel
                .0
                .edit_message(&*ctx, message)
                .with_embed(data)
                .with_components(buttons);

            if let Err(why) = edit_message.await {
                log::error!("Cannot update voice message:\n{:#?}", why);
            }
        });
    }

    fn update_list_database(&self) {
        let id = self.id;
        let list = ListDbStore::from(self);
        let ctx = Arc::clone(&self.connection.ctx);

        tokio::spawn(async move {
            if let Some(db) = get_music_db(&*ctx).await {
                log_err("Cannot insert song list to db", db.insert(&id, &list));
            }
        });
    }

    #[inline]
    pub fn current(&self) -> Option<&Song> {
        match self.state {
            PlayState::Playing(idx) | PlayState::Paused(idx, _) => self.list.get(idx as _),
            _ => None,
        }
    }

    #[inline]
    pub fn current_mut(&mut self) -> Option<&mut Song> {
        match self.state {
            PlayState::Playing(idx) | PlayState::Paused(idx, _) => self.list.get_mut(idx as _),
            _ => None,
        }
    }

    #[inline]
    fn len(&self) -> u8 {
        self.list.len() as _
    }

    fn end_current(&mut self) {
        if let PlayState::Playing(index) = self.state {
            if self.len() - self.played > index {
                self.played += 1;
            }

            if index == 0 {
                self.list.front_mut().map(|s| s.end());
                self.list.rotate_left(1);
            } else if let Some(mut song) = self.list.remove(index as _) {
                song.end();
                self.list.push_back(song);
            }

            self.state.stop();
        }
    }

    async fn play_index(&mut self, mut idx: u8) {
        if let PlayState::Playing(current_idx) = self.state {
            self.end_current();
            if current_idx < idx {
                idx -= 1;
            }
        }

        let is_looping = matches!(
            self.config.loop_state,
            LoopState::OneSong | LoopState::Infinitive if self.len() == 1
        );

        if let Some(song) = self.list.get_mut(idx as _) {
            song.volume(self.config.volume);
            song.play(&self.connection).await;

            if is_looping {
                song.enable_loop();
            }

            self.state = PlayState::Playing(idx);
        }
    }

    pub(crate) async fn play_song_index(&mut self, song_idx: u8) {
        let mut index = None;

        for (i, song) in self.list.iter().enumerate() {
            if song.index == song_idx {
                index = Some(i);
                break;
            }
        }

        if let Some(idx) = index {
            self.play_index(idx as _).await
        }
    }

    pub async fn play(&mut self, song: &str, requester: &str) -> crate::Result<()> {
        if song.trim().is_empty() {
            if let PlayState::Paused(index, _) = self.state {
                if let Some(song) = self.current() {
                    song.unpause();
                    self.state = PlayState::Playing(index);
                    self.update_message();
                }
            }

            return Ok(());
        }

        if song.starts_with("--") && song.len() > 14 {
            if let Ok(id) = u64::from_str_radix(&song[2..], 16) {
                return self.play_list(ListId(id), requester).await;
            }
        }

        if self.inc_index == u8::MAX {
            return Err("Max list reached".into());
        }

        let track = Song::new(song, requester, self.inc_index).await?;
        let name = track.name().to_owned();
        let song_index = track.index;
        let index = match self.add(track) {
            Some(idx) => idx,
            None => return Ok(()),
        };

        let is_looping = matches!(self.config.loop_state, LoopState::Infinitive);
        if self.list.len() > 1 && is_looping {
            self.current().map(|v| v.disable_loop());
        }

        if !matches!(self.state, PlayState::Stopped(_)) {
            let message = format!(
                "Added song with index {} to  the queue\n{}",
                song_index, name
            );

            self.connection.send_message(message);
        } else {
            let mut message = CreateEmbed::default();
            message.title("Now playing");
            message.description(name);
            self.connection.send_message(message);
            self.play_index(index).await;
        }

        self.inc_index += 1;
        self.update_message();
        self.update_list_database();

        Ok(())
    }

    pub async fn play_list(&mut self, id: ListId, requester: &str) -> Result<()> {
        let db = get_music_db(&*self.connection.ctx)
            .await
            .ok_or("Cannot get the music-list database")?;

        let list = db
            .get(&id)?
            .ok_or("Cannot find any list with the given ID")?;

        let free_space = MAX_LIST - self.len();
        if list.songs.len() as u8 > free_space {
            return Err(format!(
                "The list has {} more songs than the allowed number for a list",
                list.songs.len() as u8 - free_space
            )
            .into());
        }

        if list.songs.len() as u8 > u8::MAX - self.inc_index {
            return Err("Out of index".into());
        }

        let first_idx = self.inc_index;

        for song in list.songs {
            let song = Song {
                index: self.inc_index,
                requester: requester.to_owned(),
                track: TrackState::Ended {
                    url: song.url,
                    name: song.name,
                    duration: song.duration,
                },
            };

            self.add(song);

            self.inc_index += 1;
        }

        if matches!(self.state, PlayState::Stopped(_)) {
            self.play_song_index(first_idx).await;
        }

        self.update_message();
        self.update_list_database();

        Ok(())
    }

    pub async fn play_next(&mut self) -> crate::Result<()> {
        self.end_current();

        let next_index = self.next_index();

        match next_index {
            Some(index) => {
                self.play_index(index).await;
            }
            _ => {
                self.connection.send_message("Nothing left to play");
                self.state.stop();
            }
        };

        self.update_message();
        Ok(())
    }

    pub async fn play_previous(&mut self) -> bool {
        if self.len() < 2 {
            return false;
        }

        if self.len() == 2 && self.current().is_none() {
            return false;
        }

        self.current_mut().map(|v| v.end());
        self.list.rotate_right(1);

        if let Some(song) = self.list.get_mut(0) {
            song.play(&self.connection).await;
            self.state = PlayState::Playing(0);
        }

        self.update_message();
        true
    }

    fn add(&mut self, song: Song) -> Option<u8> {
        let has_song = self.list.iter().find(|v| v.name() == song.name());
        if let Some(index) = has_song.map(|v| v.index) {
            let message = format!("This song is already on the list at index {}", index);

            self.connection.send_message(message);
            return None;
        }

        if self.len() > MAX_LIST {
            self.connection
                .send_message("The queue is full, please remove a song before adding new one");
            return None;
        }

        let index = if self.played == self.len() {
            self.list.push_front(song);
            0
        } else {
            let index = self.len() - self.played - 1;
            self.list.insert(index as usize, song);
            index
        };

        if let PlayState::Playing(ref mut idx) = self.state {
            if index <= *idx {
                *idx += 1;
            }
        }

        Some(index)
    }

    pub fn remove(&mut self, song_index: u8) -> bool {
        let index = match self.list.iter().position(|e| e.index == song_index) {
            Some(index) => index,
            None => {
                self.connection.send_message(
                    "Cannot remove the song because the song index isn't on the list",
                );

                return false;
            }
        };

        if let PlayState::Playing(ref mut idx) = self.state {
            if index == *idx as usize {
                self.connection
                    .send_message("The song you wish to remove is currently playing");
                return false;
            }

            if index < *idx as usize {
                *idx -= 1;
            }
        }

        if let Some(song) = self.list.remove(index) {
            let mut embed = CreateEmbed::default();

            if let Some(thumbnail) = song.thumbnail() {
                embed.thumbnail(thumbnail);
            }

            embed.title("Removed song from the queue");
            embed.description(song);

            self.connection.send_message(embed);
            return true;
        }

        false
    }

    pub fn pause(&mut self) -> bool {
        if let Some(song) = self.current() {
            if let PlayState::Playing(index) = self.state {
                song.pause();
                self.state = PlayState::Paused(index, Instant::now());
                self.update_message();
                return true;
            }
        }

        false
    }

    pub fn stop(&mut self) -> bool {
        if let Some(song) = self.current_mut() {
            song.stop();
            self.state.stop();
            self.update_message();
            return true;
        }

        false
    }

    pub async fn skip(&mut self) -> crate::Result<()> {
        self.play_next().await?;
        self.update_message();
        Ok(())
    }

    pub fn shuffle(&mut self) -> bool {
        self.config.shuffle = !self.config.shuffle;
        self.update_message();
        self.config.shuffle
    }

    pub fn looping(&mut self) -> LoopState {
        self.config.loop_state = match self.config.loop_state {
            LoopState::Infinitive => LoopState::OneSong,
            LoopState::OneSong => LoopState::None,
            LoopState::None => LoopState::Infinitive,
        };

        if let Some(song) = self.current() {
            let loop_song = matches!(
                self.config.loop_state,
                  LoopState::OneSong
                | LoopState::Infinitive if !self.has_next()
            );

            if loop_song {
                song.enable_loop();
            } else {
                song.disable_loop();
            }
        }

        self.update_message();

        self.config.loop_state
    }

    pub fn volume(&mut self, volume: f32) {
        let volume = volume.clamp(0.0, 1.0);

        if self.config.volume == volume {
            return;
        }

        self.config.volume = volume;
        if let Some(song) = self.current() {
            song.volume(volume);
        }

        self.update_message();
    }

    fn next_index(&self) -> Option<u8> {
        if self.list.is_empty() {
            return None;
        }

        if self.len() == 1 {
            return match self.config.loop_state {
                LoopState::None => None,
                _ => Some(0),
            };
        }

        let is_looping = matches!(self.config.loop_state, LoopState::Infinitive);

        if self.config.shuffle {
            let mut rng = SmallRng::from_entropy();
            let list_len = self.len();

            if list_len == self.played {
                if is_looping {
                    Some(rng.gen_range(0..list_len))
                } else {
                    None
                }
            } else if let PlayState::Playing(idx) = self.state {
                let end_index = list_len - self.played;
                let list = (0..end_index).filter(|&v| v != idx).collect::<Vec<_>>();
                list.choose(&mut rng).copied()
            } else {
                let end_index = list_len - self.played;
                Some(rng.gen_range(0..end_index))
            }
        } else {
            let last_song_index = self
                .current()
                .or_else(|| self.list.back())
                .map(|v| v.index)?;

            let mut next_song_index = 0;
            let mut next_index = 0;
            let mut min_song_index = u8::MAX;
            let mut min_index = 0;

            for (song_index, index) in self.list.iter().map(|v| v.index).zip(0..) {
                if song_index <= min_song_index {
                    min_song_index = song_index;
                    min_index = index;
                }

                if song_index > last_song_index
                    && ((song_index < next_song_index) || next_song_index == 0)
                {
                    next_song_index = song_index;
                    next_index = index;
                }
            }

            if next_song_index == 0 {
                if is_looping {
                    next_index = min_index;
                } else {
                    return None;
                }
            }

            Some(next_index)
        }
    }

    #[inline]
    fn has_next(&self) -> bool {
        self.next_index().is_some()
    }
}

impl Embedable for MusicPlayer {
    fn append(&self, embed: &mut CreateEmbed) {
        let mut description = String::new();

        let loop_state = match self.config.loop_state {
            LoopState::Infinitive => "infinitively",
            LoopState::None => "nope",
            LoopState::OneSong => "this song",
        };

        let (color, status) = match self.state {
            PlayState::Playing(_) => (0x31dcbd, "playing"),
            PlayState::Stopped(_) => (0xcf0029, "stopped"),
            PlayState::Paused(_, _) => (0xcf0029, "paused"),
        };

        let shuffle = if self.config.shuffle { "yes" } else { "no" };

        writeln!(&mut description, "Status: **{}**", status).ok();
        writeln!(&mut description, "Looping: {}", loop_state).ok();
        writeln!(&mut description, "Shuffle: {}", shuffle).ok();
        writeln!(
            &mut description,
            "Current volume: {}%",
            (self.config.volume * 100.0) as u8
        )
        .ok();
        writeln!(&mut description, "Queued: {} songs", self.len()).ok();
        writeln!(&mut description, "Played: {} songs", self.played).ok();
        writeln!(&mut description, "Currently in: <#{}>", self.channel).ok();

        if let Some(song) = self.current() {
            if let Some(handler) = song.track.handler() {
                let metadata = handler.metadata();

                if let Some(thumbnail) = metadata.thumbnail.as_ref() {
                    embed.thumbnail(thumbnail);
                }
            }

            embed.field("Current song", &song, false);
        }

        // if !self.queue.is_empty() && self.shuffle {
        //     embed.field("Next song", "random", true);
        // } else {
        //     if let Some(song) = self.queue.get(0) {
        //         embed.field("Next song", &song, false);
        //     }
        // }

        embed.color(color);
        embed.description(description);
        embed.timestamp(&chrono::Utc::now());
        embed.title("Music Player");
        embed.footer(|f| f.text(self.id));
    }
}

const MAX_PER_PAGE: usize = 5;

impl Paginator for MusicPlayer {
    fn append_page(&self, page: std::num::NonZeroUsize, embed: &mut CreateEmbed) {
        let page = page.get() - 1;

        let mut list = self
            .list
            .iter()
            .map(|v| v.index)
            .enumerate()
            .collect::<Vec<_>>();

        list.sort_by_key(|(_, index)| *index);

        let from = MAX_PER_PAGE * page;
        let current_index = self.current().map(|v| v.index);

        list.get(from..)
            .into_iter()
            .flatten()
            .take(MAX_PER_PAGE)
            .filter_map(|(idx, _)| self.list.get(*idx))
            .for_each(|v| {
                let mut title = format!("#{}", v.index);

                if current_index.filter(|idx| *idx == v.index).is_some() {
                    title.push_str(" Currently playing");
                }

                embed.field(title, v, false);
            });
    }

    fn total_pages(&self) -> Option<usize> {
        Some((self.list.len() as f64 / MAX_PER_PAGE as f64).ceil() as usize)
    }
}
