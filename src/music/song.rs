use super::player::Connection;
use crate::storages::MusicKey;
use crate::utils;
use crate::Result;
use serenity::model::id::GuildId;
use serenity::builder::CreateSelectMenuOption;
use serenity::prelude::Context;
use serenity_ext::{CreateEmbed, Embedable};
use songbird::events::{
    Event as VoiceEvent, EventContext, EventHandler as VoiceEventHandler, TrackEvent,
};
use songbird::input::Restartable;
use songbird::tracks::{Track as SongbirdTrack, TrackHandle};
use std::fmt;
use std::sync::Arc;
use std::time::Duration;

pub(super) enum TrackState {
    WithHandler(TrackHandle, SongbirdTrack),
    OnlyHandler(TrackHandle),
    Ended {
        url: String,
        name: String,
        duration: Duration,
    },
}

impl TrackState {
    #[inline]
    pub fn handler(&self) -> Option<&TrackHandle> {
        match self {
            Self::WithHandler(ref handler, _) | Self::OnlyHandler(ref handler) => Some(handler),
            _ => None,
        }
    }

    pub fn take_song(&mut self) -> Option<SongbirdTrack> {
        if matches!(self, Self::OnlyHandler(_) | Self::Ended { .. }) {
            return None;
        }

        let handler = self.handler()?.to_owned();
        let s = core::mem::replace(self, Self::OnlyHandler(handler));
        match s {
            Self::WithHandler(_, track) => Some(track),
            _ => unreachable!(),
        }
    }

    pub fn end(&mut self) -> Result<()> {
        if let Some(handler) = self.handler() {
            let metadata = handler.metadata();
            let url = metadata
                .source_url
                .to_owned()
                .ok_or("Something wrong with the music...")?;

            let duration = metadata
                .duration
                .to_owned()
                .ok_or("Cannot get the duration of the music...")?;

            let name = metadata
                .title
                .to_owned()
                .ok_or("Something wrong with the music...")?;

            *self = Self::Ended {
                url,
                name,
                duration,
            };
        }

        Ok(())
    }
}

pub struct Song {
    pub(super) index: u8,
    pub(super) requester: String,
    pub(super) track: TrackState,
}

impl Song {
    pub async fn new(query: &str, author: &str, index: u8) -> Result<Self> {
        let data = if query.starts_with("https://") {
            let query = query.to_owned();
            Restartable::ytdl(query, true).await
        } else {
            let query = query.to_owned();
            Restartable::ytdl_search(&query, true).await
        };

        let player = data.map_err(|e| format!("Cannot get the music to play:\n{:#?}", e))?;
        let (track, handler) = songbird::create_player(player.into());

        Ok(Self {
            index,
            requester: author.to_owned(),
            track: TrackState::WithHandler(handler, track),
        })
    }

    pub fn menu_option(&self) -> CreateSelectMenuOption {
        let mut opt = CreateSelectMenuOption::default();

        let label = self.name().chars().take(100).collect::<String>();
        let duration = humantime::format_duration(self.duration());

        opt.value(self.index.to_string());
        opt.label(label);
        opt.description(format!("{} - requested by {}", duration, self.requester));

        opt
    }

    pub async fn play(&mut self, connection: &Connection) -> bool {
        let song = match self.track.take_song() {
            Some(song) => song,
            _ => {
                let url = self.url().to_owned();
                let player = match Restartable::ytdl(url, false).await {
                    Ok(player) => player,
                    Err(_) => return false,
                };

                let (track, handler) = songbird::create_player(player.into());
                self.track = TrackState::OnlyHandler(handler);

                track
            }
        };

        let song_uuid = self.uuid().unwrap_or_default();
        let mut voice = connection.voice.lock().await;

        let adding_event = self.track.handler().unwrap().add_event(
            VoiceEvent::Track(TrackEvent::End),
            SongEndHandler {
                guild_id: connection.guild,
                ctx: Arc::clone(&connection.ctx),
                song_uuid,
            },
        );

        if let Err(why) = adding_event {
            log::error!("Cannot add end event handle to the song:\n{:#?}", why);
        }

        voice.play_only(song);
        true
    }

    // pub fn seek(&self, duration: Duration) -> bool {
    //     self.player.as_ref().map(move |p| p.seek_time(duration)).is_some()
    // }

    #[inline]
    pub fn pause(&self) -> bool {
        self.track
            .handler()
            .and_then(|handler| handler.pause().ok())
            .is_some()
    }

    #[inline]
    pub fn unpause(&self) -> bool {
        self.track
            .handler()
            .and_then(|handler| handler.play().ok())
            .is_some()
    }

    #[inline]
    pub fn stop(&mut self) -> bool {
        match self.track.handler() {
            Some(handler) => {
                let ok = handler.pause().is_ok();
                handler.disable_loop().ok();
                let is_seeked = handler.seek_time(Default::default()).is_ok();
                ok && is_seeked
            }
            None => true,
        }
    }

    pub fn end(&mut self) {
        if let Some(handler) = self.track.handler() {
            handler.stop().ok();
        }

        self.track.end();
    }

    #[inline]
    pub fn enable_loop(&self) -> bool {
        self.track
            .handler()
            .and_then(|v| v.enable_loop().ok())
            .is_some()
    }

    #[inline]
    pub fn disable_loop(&self) -> bool {
        self.track
            .handler()
            .and_then(|v| v.disable_loop().ok())
            .is_some()
    }

    #[inline]
    pub fn volume(&self, volume: f32) -> bool {
        self.track
            .handler()
            .and_then(|v| v.set_volume(volume).ok())
            .is_some()
    }

    pub fn url(&self) -> &str {
        match &self.track {
            TrackState::WithHandler(handler, _) | TrackState::OnlyHandler(handler) => {
                handler.metadata().source_url.as_deref().unwrap_or_default()
            }
            TrackState::Ended { ref url, .. } => url,
        }
    }

    pub fn name(&self) -> &str {
        match &self.track {
            TrackState::WithHandler(handler, _) | TrackState::OnlyHandler(handler) => {
                handler.metadata().title.as_deref().unwrap_or_default()
            }
            TrackState::Ended { ref name, .. } => name,
        }
    }

    pub fn duration(&self) -> Duration {
        match &self.track {
            TrackState::WithHandler(handler, _) | TrackState::OnlyHandler(handler) => {
                handler.metadata().duration.unwrap_or_default()
            }
            TrackState::Ended { duration, .. } => *duration,
        }
    }

    pub fn thumbnail(&self) -> Option<&str> {
        self.track.handler()?.metadata().thumbnail.as_deref()
    }

    #[inline]
    pub fn uuid(&self) -> Option<uuid::Uuid> {
        self.track.handler().map(|s| s.uuid())
    }
}

impl fmt::Display for Song {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "***[{}]({})***\nLength: {}\nRequested by: {}",
            self.name(),
            self.url(),
            humantime::format_duration(self.duration()),
            self.requester,
        )
    }
}

impl Embedable for Song {
    fn append(&self, embed: &mut CreateEmbed) {
        if let Some(thumbnail) = self.thumbnail() {
            embed.thumbnail(thumbnail);
        }

        embed.description(&self);
    }
}

struct SongEndHandler {
    guild_id: GuildId,
    ctx: Arc<Context>,
    song_uuid: uuid::Uuid,
}

use songbird::tracks::PlayMode;

#[async_trait::async_trait]
impl VoiceEventHandler for SongEndHandler {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<VoiceEvent> {
        if let EventContext::Track(list) = ctx {
            if let Some((track, _)) = list.first() {
                if let PlayMode::End = track.playing {
                    let manager = utils::get_data::<MusicKey>(&self.ctx).await?;

                    if let Err(why) = manager.song_end_handle(self.guild_id, self.song_uuid).await {
                        log::error!("Cannot handle song end event:\n{:#?}", why);
                    };
                }
            }
        }

        None
    }
}
