use super::player::{LoopState, PlayState};
use super::MusicPlayer;
use crate::storages::InforKey;
use crate::utils;
use serenity::model::{
    id::{ChannelId, GuildId},
    user::User,
    voice::VoiceState,
};
use serenity::prelude::Context;
use songbird::Songbird;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::{Mutex, OwnedMutexGuard, RwLock};

#[derive(Clone)]
pub struct MusicManager {
    pub(super) manager: Arc<RwLock<HashMap<GuildId, Arc<Mutex<MusicPlayer>>>>>,
    pub(super) ctx: Arc<Context>,
    voice: Arc<Songbird>,
}

impl MusicManager {
    pub async fn new(ctx: Arc<Context>) -> Self {
        let voice = songbird::get(&ctx).await.expect("Where is the songbird");

        let res = Self {
            ctx,
            voice,
            manager: Arc::new(Default::default()),
        };

        tokio::spawn(super::music_care_service(res.clone()));

        res
    }

    pub async fn update_state(&self, guild: GuildId, state: VoiceState) -> crate::Result<()> {
        let is_current_user = self
            .ctx
            .data
            .read()
            .await
            .get::<InforKey>()
            .filter(|v| v.user_id == state.user_id)
            .is_some();

        if !is_current_user {
            return Ok(());
        }

        if let Some(channel) = state.channel_id {
            let mut player = match self.get_player(guild).await {
                Some(p) if p.channel != channel => p,
                _ => return Ok(()),
            };

            player.channel = channel;
            player.update_message();
        } else {
            self.manager.write().await.remove(&guild);
        }

        Ok(())
    }

    pub async fn join(
        &self,
        guild: GuildId,
        channel: ChannelId,
        log_channel: ChannelId,
    ) -> crate::Result<Arc<Mutex<MusicPlayer>>> {
        utils::typing(&self.ctx, log_channel);
        let (manager, result) = self.voice.join(guild, channel).await;

        if let Err(why) = result {
            return Err(format!("Cannot join the voice channel:\n{:#?}", why).into());
        }

        let ctx = Arc::clone(&self.ctx);
        let player = MusicPlayer::new(ctx, manager, channel, log_channel).await?;
        let player_arc = Arc::new(Mutex::new(player));
        self.manager
            .write()
            .await
            .insert(guild, Arc::clone(&player_arc));

        Ok(player_arc)
    }

    pub async fn leave(&self, guild: GuildId) -> crate::Result<()> {
        if self.voice.get(guild).is_some() {
            self.manager.write().await.remove(&guild);
        } else {
            return Err("Not in a voice channel".into());
        }

        Ok(())
    }

    pub async fn play(
        &self,
        guild: GuildId,
        log_channel: ChannelId,
        song: &str,
        requester: &User,
    ) -> crate::Result<()> {
        utils::typing(&self.ctx, log_channel);
        if self.voice.get(guild).is_some() {
            let mut player = self
                .get_player(guild)
                .await
                .ok_or("Cannot get the player")?;
            player.play(song, &requester.name).await?;
        } else {
            match utils::get_user_voice_channel(&self.ctx, guild, requester.id).await {
                Some(channel) => {
                    let player = self.join(guild, channel, log_channel).await?;
                    player.lock().await.play(song, &requester.name).await?;
                }

                None => return Err("Which channel to play in?".into()),
            }
        };

        Ok(())
    }

    #[inline]
    pub async fn get(&self, guild: GuildId) -> Option<Arc<Mutex<MusicPlayer>>> {
        self.manager.read().await.get(&guild).cloned()
    }

    // this should be dropped ASAP
    pub async fn get_player(&self, guild: GuildId) -> Option<OwnedMutexGuard<MusicPlayer>> {
        let player = self.get(guild).await?;
        let lock = Mutex::lock_owned(player).await;
        Some(lock)
    }

    pub async fn pause(&self, guild: GuildId) -> Option<bool> {
        let mut player = self.get_player(guild).await?;
        let state = player.pause();
        Some(state)
    }

    pub async fn stop(&self, guild: GuildId) -> Option<bool> {
        let mut player = self.get_player(guild).await?;
        let state = player.stop();
        Some(state)
    }

    pub async fn remove(&self, guild: GuildId, idx: u8) -> Option<bool> {
        let mut player = self.get_player(guild).await?;
        let state = player.remove(idx);
        Some(state)
    }

    pub async fn previous(&self, guild: GuildId) -> Option<bool> {
        let mut player = self.get_player(guild).await?;
        let state = player.play_previous().await;
        Some(state)
    }

    pub async fn skip(&self, guild: GuildId) -> Option<bool> {
        self.get_player(guild).await?.skip().await.is_ok().into()
    }

    pub async fn looping(&self, guild: GuildId) -> Option<LoopState> {
        let mut player = self.get_player(guild).await?;
        let state = player.looping();
        Some(state)
    }

    pub async fn shuffle(&self, guild: GuildId) -> Option<bool> {
        let mut player = self.get_player(guild).await?;
        let state = player.shuffle();
        Some(state)
    }

    pub async fn volume(&self, guild: GuildId, volume: f32) -> Option<()> {
        let mut player = self.get_player(guild).await?;
        player.volume(volume);
        Some(())
    }

    pub(super) async fn song_end_handle(
        &self,
        guild: GuildId,
        uuid: uuid::Uuid,
    ) -> crate::Result<()> {
        let mut player = match self.get_player(guild).await {
            Some(player) => player,
            None => return Err("Ended".into()),
        };

        if matches!(player.state, PlayState::Stopped(_)) {
            return Ok(());
        }

        let uuid_check = player
            .current()
            .and_then(|song| song.uuid())
            .filter(|u| u == &uuid)
            .is_some();

        if !uuid_check {
            return Ok(());
        }

        if !utils::is_dead_channel(&*self.ctx, player.channel).await {
            player.play_next().await?;
        } else {
            player
                .connection
                .send_message("Leaving due to inactive channel");
            drop(player);
            self.manager.write().await.remove(&guild);
        }

        Ok(())
    }
}
