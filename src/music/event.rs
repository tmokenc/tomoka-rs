use super::player::{MusicPlayer, PlayState};
use super::MusicManager;
use crate::utils::get_user_voice_channel;
use std::future::Future;
use std::sync::Arc;
use serenity::model::event::Event;
use serenity::model::id::UserId;
use serenity::model::interactions::{Interaction, InteractionResponseType};
use serenity::model::interactions::message_component::MessageComponentInteraction;
use serenity::prelude::Context;
use serenity_ext::RawEventHandlerRef;

pub struct MusicEventHandler {
    manager: MusicManager,
}

impl MusicEventHandler {
    pub fn new(manager: MusicManager) -> Self {
        Self { manager }
    }
}

#[async_trait::async_trait]
impl RawEventHandlerRef for MusicEventHandler {
    async fn raw_event_ref(&self, ctx: &Context, ev: &Event) {
        match ev {
            Event::InteractionCreate(e) => {
                if let Interaction::MessageComponent(ref data) = e.interaction {
                    handle_message_component(ctx, self.manager.clone(), data).await;
                }
            }

            Event::VoiceStateUpdate(event) => {
                if let Some(guild) = event.guild_id {
                    if let Err(why) = self
                        .manager
                        .update_state(guild, event.voice_state.to_owned())
                        .await
                    {
                        log::error!("Cannot update voice state:\n{:#?}", why);
                    }
                }
            }

            _ => {}
        }
    }
}
pub fn user_has_control(player: &MusicPlayer, user: UserId) -> impl Future<Output = bool> {
    let channel = player.channel;
    let guild = player.connection.guild;
    let ctx = Arc::clone(&player.connection.ctx);

    async move {
        get_user_voice_channel(&ctx, guild, user)
            .await
            .filter(|&c| c == channel)
            .is_some()
    }
}


async fn handle_message_component(
    ctx: &Context,
    manager: MusicManager,
    interaction: &MessageComponentInteraction,
) {
    let guild_id = match interaction.guild_id {
        Some(guild) => guild,
        None => return,
    };

    let mut player = match manager.get_player(guild_id).await {
        Some(player) => player,
        None => return,
    };

    if player.connection.log_message != interaction.message.id {
        return;
    }

    let check_user = user_has_control(&player, interaction.user.id);
    if !check_user.await {
        return;
    }

    let response = interaction.create_interaction_response(ctx, |resp| {
        resp.kind(InteractionResponseType::DeferredUpdateMessage)
    });

    if let Err(why) = response.await {
        log::error!("Cannot response to an interaction\n{:#?}", why);
    }

    match interaction.data.custom_id.as_str() {
        "SongSelect" => {
            let maybe_idx = interaction
                .data
                .values
                .get(0)
                .and_then(|v| v.parse::<u8>().ok());

            if let Some(idx) = maybe_idx {
                player.play_song_index(idx).await;
            }
        }

        "Previous" => {
            player.play_previous().await;
        }
        "Play/Pause" => {
            match player.state {
                PlayState::Playing(_) => {
                    player.pause();
                }
                PlayState::Paused(_, _) => {
                    player.play("", "").await.ok();
                }
                _ => {
                    player.play_next().await.ok();
                }
            };
        }
        "Play" => {
            player.play("", "").await.ok();
        }
        "Pause" => {
            player.pause();
        }
        "Stop" => {
            player.stop();
        }
        "Next" => {
            player.skip().await.ok();
        }
        "Shuffle" => {
            player.shuffle();
        }
        "Loop" => {
            player.looping();
        }
        "Volume Up" => {
            let volume = player.config.volume + 0.1;
            player.volume(volume);
        }
        "Volume Down" => {
            let volume = player.config.volume - 0.1;
            player.volume(volume);
        }
        _ => return,
    }
}