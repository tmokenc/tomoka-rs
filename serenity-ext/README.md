# Tomoka Serenity Extension

This is an extension for the async serenity. <br>
Provide a `closure`-less life, which is an easier life.

## ChannelExt

Since the async closure isn't stable yet, this won't work
```rs
async fn send_future_content(ctx: &Context, msg: &Message) -> CommandResult {
    let fut = /* some thing that is a Future */;
    msg.channel_id.send_message(ctx, |message| {
        message.content(fut.await)
    }).await?;
    
    Ok(())
}
```

We have to get that description outside of the closure then bring it in.
Like this
```rs
async fn send_future_content(ctx: &Context, msg: &Message) -> CommandResult {
    let fut = /* some thing that is a Future */;
    let content = fut.await;
    msg.channel_id.send_message(ctx, |message| {
        message.content(content)
    }).await?;
    
    Ok(())
}
```

But with this trait, it's just simple as this

```rs
use tomo_serenity_ext::ChannelExt;

async fn send_future_content(ctx: &Context, msg: &Message) -> CommandResult {
    let fut = /* some thing that is a Future */;
    
    ChannelExt::send_message(msg.channel_id, ctx) // Do this because the ChannelId itself has the `send_message` method
        .with_content(fut.await)
        .await?;
    
    Ok(())
}
```

It does look better with the `.await` at the end as well.<br>
Even better, sending a embed
```rs
use tomo_serenity_ext::ChannelExt;

async fn send_future_content(ctx: &Context, msg: &Message) -> CommandResult {
    let fut = /* some thing that is a Future */;
    
    msg.channel_id
        .send_embed(ctx)
        .with_description(fut.await)
        .await?;
    
    Ok(())
}
```

## Embedable

This trait exists thanks to my laziness (I want to put all of the embed [in one place](https://gitlab.com/tmokenc/tomoka-rs/-/blob/master/src/traits.rs) for easier to maintain)

```rs
use tomo_serenity_ext::{CreateEmbed, Embedable}; // The `CreateEmbed` is reexported from `serenity`

struct Foo;

impl Embedable for Foo {
    fn append_to<'a>(&self, &'a mut CreateEmbed) -> &'a mut CreateEmbed {
        embed.description("Bar").color(0xfafafa)
    }
}
```

That's all for the implementation, use it like this

```rs
use tomo_serenity_ext::Embedable;

async fn send_embed(ctx: &Context, msg: &Message) -> CommandResult {
    let foo = Foo;
    
    foo.send_embed(ctx, msg.channel_id).await?;
    
    Ok(())
}
```

Or with the traditional serenity

```rs
use tomo_serenity_ext::Embedable;

async fn send_embed(ctx: &Context, msg: &Message) -> CommandResult {
    let foo = Foo;
    
    msg.channel_id
        .send_message(ctx, |m| m.embed(|e| foo.append_to(e)))
        .await?;
    
    Ok(())
}
```

Or with the `ChannelExt` trait

```rs
use tomo_serenity_ext::ChannelExt;

async fn send_embed(ctx: &Context, msg: &Message) -> CommandResult {
    let foo = Foo;
    
    ChannelExt::send_message(msg.channel_id, ctx)
        .with_embed(foo)
        .await?;
    
    Ok(())
}
```

## Paginator

Similar to the `Embedable` trait, but here we have a `page` and the `self` is a `&mut self`

```rs
use tomo_serenity_ext::paginator::{Paginator, NonZeroUsize, CreateEmbed};

struct Foo {
    bar: usize
}

impl Paginator for Foo {
    fn append_page(&mut self, page: NonZeroUsize, embed: &mut CreateEmbed) {
        embed.description(format!("Current page is {}", self.bar));
        *self.bar += 1;
    }
}

async fn send_pagination(ctx: &Context, msg: &Message) -> CommandResult {
    let mut foo = Foo {
        bar: 0
    };
    
    foo.pagination(ctx, msg).await?;
    
    Ok(())
}
```
Simple as that

    
# Todo
- Command trait (A serenity framework that based on this single trait)
- More method on the `ChannelExt`, like `edit_message`, `fetch_and_edit`, ...
- Make the `Paginator` trait better