use crate::remove_components;
pub use crate::CreateEmbed;
use crate::{ChannelExt, Embedable, Result};
use core::borrow::Borrow;
pub use core::num::NonZeroUsize;
use core::str::FromStr;
use core::time::Duration;
use futures::stream::StreamExt;
use magic::types::Void;
use serenity::builder::{CreateActionRow, CreateButton, CreateComponents};
use serenity::client::Context;
use serenity::collector::ComponentInteractionCollectorBuilder;
use serenity::model::channel::{Message, ReactionType};
use serenity::model::id::{ChannelId, MessageId, UserId};
use serenity::model::interactions::{message_component::ButtonStyle, InteractionResponseType};
use std::convert::TryInto;
use std::sync::Arc;
use tokio::time::timeout;

fn paginator_buttons(opt: PaginatorButtonOption) -> CreateComponents {
    let mut components = CreateComponents::default();
    const BUTTONS: [(&str, &str); 5] = [
        ("⏮️", "First"),
        ("◀️", "Previous"),
        ("▶️", "Next"),
        ("⏭️", "Last"),
        ("❌", "Close"),
    ];

    let [mut first, mut pre, mut next, mut last, mut close]: [CreateButton; 5] = BUTTONS
        .iter()
        .map(|(icon, text)| {
            let mut button = CreateButton::default();
            button.style(ButtonStyle::Secondary);
            button.emoji(ReactionType::Unicode(String::from(*icon)));
            button.label(text);
            button.custom_id(text);
            button
        })
        .collect::<Vec<_>>()
        .try_into()
        .unwrap();

    if opt.current == 1 {
        first.disabled(true);
        pre.disabled(true);
    }

    if let Some(total) = opt.total {
        if total == opt.current {
            last.disabled(true);
            next.disabled(true);
        }
    } else {
        last.disabled(true);
    }

    close.style(ButtonStyle::Danger);

    let mut action_row = CreateActionRow::default();

    for button in vec![first, pre, next, last, close] {
        action_row.add_button(button);
    }

    components.add_action_row(action_row);
    components
}

struct PaginatorButtonOption {
    current: usize,
    total: Option<usize>,
}

#[derive(Debug, Clone, Copy)]
pub enum PaginatorAction {
    Page(usize),
    First,
    Previous,
    Next,
    Last,
    Destroy,
}

impl FromStr for PaginatorAction {
    type Err = Void;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let res = match s.to_lowercase().trim() {
            "first" => Self::First,
            "previous" => Self::Previous,
            "next" => Self::Next,
            "last" => Self::Last,
            "close" | "destroj" => Self::Destroy,
            s if s.starts_with("page") => Self::Page(s[4..].trim().parse()?),
            _ => return Err(Void),
        };

        Ok(res)
    }
}

pub struct PaginatorOption {
    pub channel_id: ChannelId,
    pub user: UserId,
    pub message: Option<MessageId>,
}

impl PaginatorOption {
    #[inline]
    pub fn new(channel_id: ChannelId, user: UserId) -> Self {
        Self {
            channel_id,
            user,
            message: None,
        }
    }

    pub fn new_with_reference(channel_id: ChannelId, user: UserId, message: MessageId) -> Self {
        Self {
            channel_id,
            user,
            message: Some(message),
        }
    }
}

impl From<&Message> for PaginatorOption {
    fn from(m: &Message) -> Self {
        Self {
            channel_id: m.channel_id,
            user: m.author.id,
            message: Some(m.id),
        }
    }
}

#[async_trait]
pub trait Paginator {
    /// Notice that the page start at 1
    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed);

    #[inline]
    fn default_page(&self) -> NonZeroUsize {
        unsafe { NonZeroUsize::new_unchecked(1) }
    }

    #[inline]
    fn total_pages(&self) -> Option<usize> {
        None
    }

    #[inline]
    fn timeout(&self) -> Duration {
        Duration::from_secs(30)
    }

    async fn pagination<C, O>(&self, ctx: C, opt: O) -> Result<()>
    where
        C: Borrow<Context> + Send,
        O: Into<PaginatorOption> + Send,
        Self: Send + Sync,
    {
        let ctx = ctx.borrow();
        let opt = opt.into();
        let total = self.total_pages();

        let http = Arc::clone(&ctx.http);

        match total {
            Some(0) => return Ok(()),
            Some(1) => {
                let page = unsafe { NonZeroUsize::new_unchecked(1) };

                let mut send_embed = opt.channel_id.send_embed(ctx);
                self.append_page(page, send_embed.inner_embed());

                if let Some(id) = opt.message {
                    send_embed = send_embed.with_reference_message((opt.channel_id, id));
                }

                send_embed.await?;

                return Ok(());
            }
            _ => (),
        };

        let mut current_page = self.default_page().get();

        let mut send_page = ChannelExt::send_message(&opt.channel_id, ctx)
            .with_embed({
                let page = NonZeroUsize::new(current_page).unwrap();
                let mut embed = CreateEmbed::default();
                self.append_page(page, &mut embed);
                embed
            })
            .with_components(paginator_buttons(PaginatorButtonOption {
                total,
                current: current_page,
            }));

        if let Some(id) = opt.message {
            send_page = send_page.with_reference_message((opt.channel_id, id));
        }

        let Message { channel_id, id, .. } = send_page.await?;

        let mut stream = ComponentInteractionCollectorBuilder::new(ctx)
            .message_id(id)
            .channel_id(channel_id)
            .author_id(opt.user)
            .await;

        while let Ok(Some(interaction)) = timeout(self.timeout(), stream.next()).await {
            let action = match interaction.data.custom_id.parse::<PaginatorAction>() {
                Ok(a) => a,
                _ => continue,
            };

            match action {
                PaginatorAction::First if current_page > 1 => current_page = 1,

                PaginatorAction::Previous if current_page > 1 => current_page -= 1,

                PaginatorAction::Next => match total {
                    Some(max) if current_page < max => current_page += 1,
                    _ => continue,
                },

                PaginatorAction::Last => match total {
                    Some(max) if current_page != max => current_page = max,
                    _ => continue,
                },

                PaginatorAction::Page(page) if current_page != page => match total {
                    Some(max) if page <= max => current_page = page,
                    _ => current_page = page,
                },

                PaginatorAction::Destroy => {
                    http.delete_message(channel_id.0, id.0).await?;
                    return Ok(());
                }

                _ => continue,
            }

            let page_opt = PaginatorButtonOption {
                current: current_page,
                total: self.total_pages(),
            };

            let page = NonZeroUsize::new(current_page).unwrap();

            let components = paginator_buttons(page_opt);
            let mut embed = CreateEmbed::default();
            self.append_page(page, &mut embed);

            let resp = interaction.create_interaction_response(ctx, move |resp| {
                resp.kind(InteractionResponseType::UpdateMessage);
                resp.interaction_response_data(move |d| {
                    d.add_embed(embed);
                    d.components(|c| {
                        c.0 = components.0;
                        c
                    })
                })
            });

            if let Err(why) = resp.await {
                log::error!("Cannot response to an input\n{:#?}", why);
            }
        }

        remove_components(ctx, channel_id, id).await?;

        Ok(())
    }
}

impl<E: Embedable> Paginator for Vec<E> {
    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed) {
        let page = page.get();
        embed.footer(|f| f.text(format!("{} / {}", page, self.len())));
        match self.get(page - 1) {
            Some(data) => data.append_to(embed),
            None => embed.description("This page does not exist"),
        };
    }

    #[inline]
    fn total_pages(&self) -> Option<usize> {
        Some(self.len())
    }
}

#[async_trait]
impl<P: Paginator> Paginator for tokio::sync::Mutex<P> {
    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed) {
        tokio::runtime::Handle::current().block_on(async {
            self.lock().await.append_page(page, embed);
        });
    }

    fn total_pages(&self) -> Option<usize> {
        tokio::runtime::Handle::current().block_on(async { self.lock().await.total_pages() })
    }
}
