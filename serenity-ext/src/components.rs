use serde::ser::{Serialize, Serializer};

pub enum Component {
    ActionRow(ActionRow),
    Button(Button),
}

impl Component {
    pub fn is_empty(&self) -> bool {
        matches!(self, Self::ActionRow(r) if r.is_empty())
    }
}

impl Serialize for Component {
    fn serialize<S: Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
        let mut state = s.serialize_map(None);
        
        match self {
            Self::ActionRow(row) => {
                state.serialize_entry(&"type", 1)?;
                state.serialize_entry(&"components", row)?;
            }
            
            Self::Button(btn) => {
                state.serialize_entry(&"type", 2)?;
                state.serialize_entry(&"style", match btn.style {
                    ButtonStyle::Primary => 1,
                    ButtonStyle::Secondary => 2,
                    ButtonStyle::Success => 3,
                    ButtonStyle::Danger => 4,
                    ButtonStyle::Link => 5,
                })?;
            }
        }
        
        state.end()
    }
}

pub struct ActionRow {
    components: Vec<Component>,
}

impl From<ActionRow> for Component {
    fn from(s: ActionRow) -> Self {
        Self::ActionRow(s)
    }
}

pub struct Button {
    style: ButtonStyle,
    label: Option<String>,
    emoji: Option<Emoji>,
    custom_id: Option<String>,
    url: Option<String>,
    disabled: bool,
}

impl From<Button> for Component {
    fn from(s: Button) -> Self {
        Self::Button(s)
    }
}

pub struct Emoji {
    id: u64,
    name: Option<String>,
    animated: Option<bool>,
}

/// https://discord.com/developers/docs/interactions/message-components#buttons-button-styles
pub enum ButtonStyle {
    Primary,
    Secondary,
    Success,
    Danger,
    Link
}

#[inline]
pub fn action_row() -> Component {
    Component::ActionRow(Vec::new())
}