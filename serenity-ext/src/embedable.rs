use crate::channelext::{ChannelExt, SendEmbedBuilder, SerenityHttp};
pub use crate::CreateEmbed;
use serenity::model::channel::Embed;
use serenity::model::id::ChannelId;

/// This trait exist due to the number of rewriting thanks to my stupid code
pub trait Embedable {
    fn append(&self, embed: &mut CreateEmbed);
    fn append_to<'a>(&self, embed: &'a mut CreateEmbed) -> &'a mut CreateEmbed {
        self.append(embed);
        embed
    }

    fn embed_data(&self) -> CreateEmbed {
        let mut embed = CreateEmbed::default();
        self.append(&mut embed);
        embed
    }

    fn send_embed(&self, http: impl SerenityHttp, channel_id: ChannelId) -> SendEmbedBuilder<'_> {
        let embed = self.embed_data();
        channel_id.send_embed(http).with_embed(embed)
    }
}

impl Embedable for CreateEmbed {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.0 = self.0.to_owned();
    }

    #[inline]
    fn embed_data(&self) -> CreateEmbed {
        self.to_owned()
    }
}

impl Embedable for Embed {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.color(self.colour);

        if let Some(ref author) = self.author {
            embed.author(|a| {
                a.name(&author.name);
                if let Some(ref icon) = author.icon_url {
                    a.icon_url(icon);
                }
                if let Some(ref url) = author.url {
                    a.url(url);
                }

                a
            });
        }

        for field in self.fields.iter() {
            embed.field(&field.name, &field.value, field.inline);
        }

        if let Some(ref footer) = self.footer {
            embed.footer(|f| {
                f.text(&footer.text);

                if let Some(ref icon) = footer.icon_url {
                    f.icon_url(icon);
                }

                f
            });
        }

        if let Some(ref desc) = self.description {
            embed.description(desc);
        }

        if let Some(ref image) = self.image {
            embed.image(&image.url);
        }

        if let Some(ref thumbnail) = self.thumbnail {
            embed.thumbnail(&thumbnail.url);
        }

        if let Some(ref timestamp) = self.timestamp {
            embed.timestamp(timestamp.as_str());
        }

        if let Some(ref title) = self.title {
            embed.title(title);
        }

        if let Some(ref url) = self.url {
            embed.url(url);
        }
    }
}

impl Embedable for String {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.description(self);
    }
}

impl Embedable for &str {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.description(self);
    }
}
