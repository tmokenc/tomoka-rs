use std::collections::HashMap;
use crate::RawEventHandlerRef;
use crate::events::Event;
use serenity::prelude::Context;
use serenity::http::client::Http;
use std::sync::Arc;

pub struct SlashCommand {
    name: String,
    description: String,
    usage: String,
}

impl SlashCommand {
    pub fn json(&self) -> serde_json::Value {
        Default::default();
    }
}

pub struct CommandEventHandler {
    slash: HashMap<String, SlashCommand>,
    http: Arc<Http>,
}

impl CommandEventHandler {
    pub fn new(http: Arc<Http>) -> Self {
        Self {
            slash: Default::default(),
            http,
        }
    }
    
    pub async fn add_slash_command(&mut self, cmd: SlashCommand) ->  {
        let cmd = self
            .http
            .create_global_application_command(cmd.json())
            .await?;
            
        self.slash.insert(cmd.name.to_owned(), cmd);
    }
    
    pub async fn remove_slash_command(&mut self, cmd: &str) {
        self.slash.remove(cmd);
    }
}

#[async_trait::async_trait]
impl RawEventHandlerRef for SlashCommandHandler {
    async raw_event_ref(&self, ctx: &Context, ev: &Event) {
        match ev {
            _ => ();
        }
    }
}