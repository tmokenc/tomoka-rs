#[macro_use]
extern crate async_trait;

use std::error::Error;

pub(crate) type Result<T> = core::result::Result<T, Box<dyn Error + Send + Sync>>;

pub mod channelext;
pub mod embedable;
pub mod events;
pub mod paginator;

pub use channelext::{ChannelExt, Color};
pub use embedable::Embedable;
pub use events::{MultiRawHandler, RawEventHandlerRef};
pub use paginator::{Paginator, PaginatorOption};

pub use serenity::builder::CreateEmbed;

use serenity::builder::CreateComponents;
use serenity::model::id::{ChannelId, MessageId};
use serenity::prelude::Context;

pub async fn remove_components(
    ctx: &Context,
    channel: ChannelId,
    message: MessageId,
) -> Result<()> {
    let mut msg = channel.message(ctx, message).await?;

    msg.embeds.truncate(1);

    let maybe_embed = msg.embeds.pop();
    let empty_component = CreateComponents::default();

    let mut edit = channel
        .0
        .edit_message(ctx, message)
        .with_content(msg.content)
        .with_components(empty_component);

    if let Some(embed) = maybe_embed {
        edit = edit.with_embed(embed);
    }

    edit.await?;

    Ok(())
}
