use wasm_bindgen::prelude::*;
use gloo_storage::{Storage, LocalStorage};
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Routable, PartialEq, Clone, Copy, Debug)]
enum Route {
    #[at("/commands")]
    Commands,

    #[at("/control")]
    Control,

    #[at("/")]
    Index,

    #[not_found]
    #[at("/404")]
    NotFound,
}

impl Route {
    fn render(&self) -> Html {
        match self {
            Self::NotFound => html! {
                <h1>{ "404 Successfully NOT FOUND" }</h1>
            },

            Self::Index => html! {
                <h1>{ "Hi, I'm Tomoka" }</h1>
            },

            Self::Commands => html! {
                <h1>{ "To be implemented" }</h1>
            },

            Self::Control => html! {
                <h1>{ "To be implemented" }</h1>
            },
        }
    }
}

enum Msg {
    Auth(User),
    LogOut,
    UpdateConfig,
}

struct User {
    token: String,
    ava: String,
    name: String,
}

struct App {
    user: Option<User>,
    updating: bool,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        // if let Some(token) = LocalStorage.get::<String>("token") {
        //     ctx.link.send_future(async {
//
        //     });
        // }

        Self {
            user: None,
            updating: false,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Auth(user) => {
                self.user = Some(user);
                true
            }

            Msg::LogOut => {
                self.user = None;
                log::info!("Logged Out");
                true
            }

            Msg::UpdateConfig => {
                log::info!("Request for update config");
                false
            }
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <div>
                <BrowserRouter>
                    <Switch<Route> render={Switch::render(Route::render)} />
                </BrowserRouter>
            </div>
        }
    }
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    wasm_logger::init(Default::default());
    yew::start_app::<App>();
    Ok(())
}
