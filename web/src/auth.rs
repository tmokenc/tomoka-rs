use gloo_storage::{Storage, LocalStorage};

pub struct User {
    name: String,
    hash_tag: String,
    discord_id: String,
}

impl User {
    const KEY: &'static str = "user";
    const JWT_KEY: &'static str = "jwt";

    pub fn get() -> Option<Self> {
        let jwt = LocalStorage.get::<String>(Self::KEY)?;
    }

    pub fn log_out() {
        LocalStorage.remove(Self::KEY);
    }

    // pub fn log_in() -> Result<Self, wasm_bindgen::JsValue> {
//
    // }
}
