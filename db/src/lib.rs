use bincode::config::{BigEndian, DefaultOptions, Options, WithOtherEndian};
use lazy_static::lazy_static;
use log::error;
use serde::de::DeserializeOwned;
use serde::ser::Serialize;
use sled::{Db, Tree};
use std::error::Error;
use std::marker::PhantomData;
use std::path::Path;
use std::sync::Arc;

type Manager = Arc<Db>;
type Result<T> = std::result::Result<T, Box<dyn Error + Sync + Send>>;
type Encoder = WithOtherEndian<DefaultOptions, BigEndian>;

lazy_static! {
    static ref ENCODER: Encoder = DefaultOptions::new().with_big_endian();
}

#[derive(Debug, Clone, Copy)]
pub enum EncodingType {
    Bincode,
    Json,
    // Bson,
}

impl EncodingType {
    const JSON_PREFIX: &'static [u8] = "json_".as_bytes();

    fn from_bytes(bytes: &[u8]) -> Self {
        if bytes.starts_with(Self::JSON_PREFIX) {
            Self::Json
        } else {
            Self::Bincode
        }
    }
}

pub fn serialize<T: Serialize>(value: &T, encoding: EncodingType) -> Result<Vec<u8>> {
    let bytes = match encoding {
        EncodingType::Json => serde_json::to_vec(value)?,
        EncodingType::Bincode => ENCODER.serialize(value)?,
    };

    Ok(bytes)
}

pub fn deserialize<T: DeserializeOwned, S: AsRef<[u8]>>(
    slice: S,
    encoding: EncodingType,
) -> Result<T> {
    let data = match encoding {
        EncodingType::Json => serde_json::from_slice(slice.as_ref())?,
        EncodingType::Bincode => ENCODER.deserialize(slice.as_ref())?,
    };

    Ok(data)
}

#[derive(Clone)]
/// It's safe to clone because all the inners are already inside Arcs
pub struct DbInstance {
    tree: Option<Tree>,
    manager: Manager,
}

impl Drop for DbInstance {
    fn drop(&mut self) {
        self.manager.flush().ok();
    }
}

#[derive(Debug)]
pub struct Batch {
    sled_batch: sled::Batch,
    encoding: EncodingType,
}

impl Default for Batch {
    fn default() -> Self {
        Self {
            sled_batch: Default::default(),
            encoding: EncodingType::Bincode,
        }
    }
}

impl Batch {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn json(self) -> Self {
        Self {
            encoding: EncodingType::Json,
            ..self
        }
    }

    pub fn remove<S: Serialize>(&mut self, key: &S) -> Result<()> {
        let data = serialize(key, self.encoding)?;
        self.sled_batch.remove(data);
        Ok(())
    }

    pub fn insert<K: Serialize, V: Serialize>(&mut self, key: &K, val: &V) -> Result<()> {
        let k = serialize(key, self.encoding)?;
        let v = serialize(val, self.encoding)?;
        self.sled_batch.insert(k, v);
        Ok(())
    }
}

impl From<Batch> for sled::Batch {
    fn from(b: Batch) -> sled::Batch {
        b.sled_batch
    }
}

pub struct Iter<K: DeserializeOwned, V: DeserializeOwned> {
    iter: sled::Iter,
    encoding: EncodingType,
    _marker: PhantomData<(K, V)>,
}

pub struct IterKey<K: DeserializeOwned> {
    iter: sled::Iter,
    encoding: EncodingType,
    _marker: PhantomData<K>,
}

impl<K: DeserializeOwned, V: DeserializeOwned> Iter<K, V> {
    pub(crate) fn new(iter: sled::Iter, encoding: EncodingType) -> Self {
        Self {
            iter,
            encoding,
            _marker: PhantomData,
        }
    }

    pub fn keys(self) -> IterKey<K> {
        IterKey {
            iter: self.iter,
            encoding: self.encoding,
            _marker: PhantomData,
        }
    }
}

impl<K: DeserializeOwned, V: DeserializeOwned> Iterator for Iter<K, V> {
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        let encoding = self.encoding;

        self.iter
            .by_ref()
            .filter_map(|v| v.ok())
            .find_map(|(ref key, ref val)| {
                let data: Result<(K, V)> = (|| {
                    let k = deserialize(key, encoding)?;
                    let v = deserialize(val, encoding)?;
                    Ok((k, v))
                })();

                match data {
                    Ok(e) => Some(e),
                    Err(why) => {
                        error!(
                            "Cannot deserialize data with encoding {:?} | {}",
                            encoding, why
                        );
                        None
                    }
                }
            })
    }
}

impl<K: DeserializeOwned> Iterator for IterKey<K> {
    type Item = K;

    fn next(&mut self) -> Option<Self::Item> {
        let encoding = self.encoding;

        self.iter
            .by_ref()
            .filter_map(|v| v.ok())
            .map(|(key, _)| key)
            .find_map(|ref key| match deserialize(key, encoding) {
                Ok(e) => Some(e),
                Err(why) => {
                    error!(
                        "Cannot deserialize key with encoding {:?} | {}",
                        encoding, why
                    );
                    None
                }
            })
    }
}

pub struct ClearDb {
    pub items_count: usize,
    pub total_size: usize,
}

impl DbInstance {
    pub fn new<'a, P, N>(path: P, name: N) -> Result<Self>
    where
        P: AsRef<Path>,
        N: Into<Option<&'a [u8]>>,
    {
        let manager = get_db_manager(path)?;
        let db = Self::from_manager(manager, name.into())?;
        Ok(db)
    }

    pub fn get_tree_sizes(&self) -> Vec<(sled::IVec, Result<usize>)> {
        let mut res = Vec::new();
        let root = self.root_db().size();
        res.push((Default::default(), root));
        for name in self.manager.tree_names() {
            // let key = ENCODER.deserialize(&name).unwrap();
            let size = self.open(&name).and_then(|v| v.size());
            res.push((name, size));
        }

        res
    }

    pub fn from_manager<N: AsRef<[u8]>>(manager: Manager, name: Option<N>) -> Result<Self> {
        let tree = match name {
            Some(n) => Some(manager.open_tree(n)?),
            None => None,
        };

        Ok(Self { tree, manager })
    }

    fn encoding_type(&self) -> EncodingType {
        EncodingType::from_bytes(&self.tree().name())
    }

    pub fn open<N: AsRef<[u8]>>(&self, tree: N) -> Result<Self> {
        let manager = Arc::clone(&self.manager);
        Self::from_manager(manager, Some(tree))
    }

    // this db collection store data as json instead of bincode
    pub fn open_json<N: AsRef<[u8]>>(&self, tree: N) -> Result<Self> {
        let mut name = EncodingType::JSON_PREFIX.to_owned();
        name.extend(tree.as_ref());
        self.open(name)
    }

    pub fn size(&self) -> Result<usize> {
        let tree = self.tree();
        let mut size = 0;
        for data in tree.iter() {
            let (k, v) = data?;
            size += k.len() + v.len();
        }

        Ok(size)
    }

    pub fn typed<K, V>(&self) -> TypedDbInstance<K, V>
    where
        K: Serialize + DeserializeOwned,
        V: Serialize + DeserializeOwned,
    {
        TypedDbInstance {
            db: self.clone(),
            _tag: PhantomData,
        }
    }

    pub fn into_typed<K, V>(self) -> TypedDbInstance<K, V>
    where
        K: Serialize + DeserializeOwned,
        V: Serialize + DeserializeOwned,
    {
        TypedDbInstance {
            db: self,
            _tag: PhantomData,
        }
    }

    pub fn get<K, V>(&self, key: &K) -> Result<Option<V>>
    where
        K: Serialize,
        V: DeserializeOwned,
    {
        let k = ENCODER.serialize(key)?;
        let encoding = self.encoding_type();

        let res = self
            .tree()
            .get(&k)?
            .and_then(|ref v| deserialize(v, encoding).ok());

        Ok(res)
    }

    pub fn get_all<K, V>(&self) -> Iter<K, V>
    where
        K: DeserializeOwned,
        V: DeserializeOwned,
    {
        let iter = self.tree().iter();
        Iter::<K, V>::new(iter, self.encoding_type())
    }

    pub fn get_all_keys<K: DeserializeOwned>(&self) -> IterKey<K> {
        IterKey {
            iter: self.tree().iter(),
            encoding: self.encoding_type(),
            _marker: PhantomData,
        }
    }

    pub fn insert<K, V>(&self, key: &K, value: &V) -> Result<()>
    where
        K: Serialize,
        V: Serialize,
    {
        let encoding = self.encoding_type();
        let k = serialize(key, encoding)?;
        let v = serialize(value, encoding)?;
        self.tree().insert(&k, v)?;
        Ok(())
    }

    pub fn remove<K: Serialize>(&self, key: &K) -> Result<()> {
        let k = serialize(key, self.encoding_type())?;
        self.tree().remove(&k)?;
        Ok(())
    }

    pub fn remove_many<K: Serialize, I: IntoIterator<Item = K>>(&self, keys: I) -> Result<()> {
        let mut batch = sled::Batch::default();

        for key in keys {
            let k = serialize(&key, self.encoding_type())?;
            batch.remove(k);
        }

        self.tree().apply_batch(batch)?;
        Ok(())
    }

    pub fn remove_first_n(&self, n: usize) -> Result<()> {
        let mut batch = sled::Batch::default();
        let tree = self.tree();

        for key in self.tree().iter().keys().take(n) {
            batch.remove(key?);
        }

        tree.apply_batch(batch).map_err(|e| e.into())
    }

    pub fn clear(&self) -> Result<ClearDb> {
        let tree = self.tree();
        let mut items_count = 0;
        let mut total_size = 0;

        for k in tree.iter().keys() {
            let key = k?;
            if let Some(value) = tree.remove(&key)? {
                items_count += 1;
                total_size += key.len() + value.len();
            }
        }

        Ok(ClearDb {
            items_count,
            total_size,
        })
    }

    pub fn batch(&self, batch: Batch) -> Result<()> {
        self.tree().apply_batch(batch.into())?;
        Ok(())
    }

    #[inline]
    pub fn tree(&self) -> &Tree {
        match &self.tree {
            Some(t) => t,
            None => &**self.manager,
        }
    }

    pub fn root_db(&self) -> Self {
        Self {
            tree: None,
            manager: Arc::clone(&self.manager),
        }
    }

    #[inline]
    pub fn manager(&self) -> Manager {
        Arc::clone(&self.manager)
    }
}

/// This Tagged Database, which the value should be known before hand
pub struct TypedDbInstance<K, V> {
    db: DbInstance,
    _tag: PhantomData<(K, V)>,
}

impl<K, V> AsRef<DbInstance> for TypedDbInstance<K, V> {
    fn as_ref(&self) -> &DbInstance {
        &self.db
    }
}

impl<K, V> TypedDbInstance<K, V> {
    pub fn untyped(&self) -> DbInstance {
        self.db.clone()
    }

    pub fn into_untyped(self) -> DbInstance {
        self.db
    }
}

impl<K, V> TypedDbInstance<K, V>
where
    K: Serialize + DeserializeOwned,
    V: Serialize + DeserializeOwned,
{
    pub fn get(&self, key: &K) -> Result<Option<V>> {
        self.db.get(key)
    }

    pub fn insert(&self, key: &K, value: &V) -> Result<Option<V>> {
        let encoding = self.as_ref().encoding_type();
        let k = serialize(key, encoding)?;
        let v = serialize(value, encoding)?;
        let res = self
            .db
            .tree()
            .insert(k, v)?
            .and_then(|ref v| deserialize(v, encoding).ok());

        Ok(res)
    }

    pub fn remove(&self, key: &K) -> Result<Option<V>> {
        let encoding = self.as_ref().encoding_type();
        let k = serialize(key, encoding)?;
        let res = self
            .db
            .tree()
            .remove(k)?
            .and_then(|ref v| deserialize(v, encoding).ok());

        Ok(res)
    }

    /// Update the value and return the old one
    pub fn fetch_and_update<F>(&self, key: &K, mut f: F) -> Result<Option<V>>
    where
        F: FnMut(Option<V>) -> Option<V>,
    {
        let encoding = self.as_ref().encoding_type();
        let k = serialize(key, encoding)?;
        let old_data = self.db.tree().fetch_and_update(k, move |old| {
            let value = old.and_then(|v| deserialize(v, encoding).ok());
            let res = f(value)?;
            serialize(&res, encoding).ok()
        })?;

        let old_value = old_data
            .as_ref()
            .and_then(|v| deserialize(v, encoding).ok());
        Ok(old_value)
    }

    pub fn iter(&self) -> Iter<K, V> {
        self.as_ref().get_all()
    }
}
// use tokio::runtime::Handle as TokioHandler;
// impl DbInstance {
//     #[inline]
//     pub async fn get_async<K, V>(&self, key: &K) -> Result<Option<V>>
//     where
//         K: Serialize,
//         V: DeserializeOwned,
//     {
//         TokioHandler::try_current()?.spawn_blocking(move || self.get(key))
//     }
//
//     #[inline]
//     pub async fn insert_async<K, V>(&self, key: &K, value: &V) -> Result<()>
//     where
//         K: Serialize,
//         V: Serialize,
//     {
//         TokioHandler::try_current()?.spawn_blocking(move || self.insert(key, value))
//     }
//
//     #[inline]
//     pub async fn remove_async<K: Serialize>(&self, key: &K) -> Result<()> {
//         TokioHandler::try_current()?.spawn_blocking(move || self.remove(key))
//     }
//
//     #[inline]
//     pub async fn remove_many_async<K: Serialize, I: IntoIterator<Item = K>>(&self, keys: I) -> Result<()> {
//         TokioHandler::try_current()?.spawn_blocking(move || self.remove_many(keys))
//     }
//
//     #[inline]
//     pub async fn batch_async(&self, batch: Batch) -> Result<()> {
//         TokioHandler::try_current()?.spawn_blocking(move || self.batch(batch))
//     }
//
//     #[inline]
//     pub async fn clear_async(&self) -> Result<()> {
//         TokioHandler::try_current()?.spawn_blocking(move || self.clear())
//     }
//
// }

#[inline]
pub fn get_db_manager(path: impl AsRef<Path>) -> Result<Manager> {
    sled::Config::new()
        .path(path)
        //.cache_capacity(10_000_000_000)
        // .compression_factor(10)
        .cache_capacity(512 * 1024 * 1024)
        .use_compression(true)
        .open()
        .map(Arc::new)
        .map_err(|v| Box::new(v) as Box<_>)
}

/// Retry until we able to get to database
/// Return `None` if cannot get the instance after the retry counts
/// Passing `None` to the `retry` will make the loop run forever until we have the database instance
pub async fn get_db_instance<P, R>(path: P, retry: R) -> Option<DbInstance>
where
    P: AsRef<Path>,
    R: Into<Option<usize>>,
{
    let mut retry = retry.into();
    let path = path.as_ref().to_owned();

    loop {
        match DbInstance::new(&path, None) {
            Ok(db) => break Some(db),
            Err(why) => {
                log::error!("{}", why);
                if let Some(retry) = retry.as_mut() {
                    if *retry == 0 {
                        break None;
                    }

                    *retry -= 1;
                }

                let wait = core::time::Duration::from_millis(500);
                tokio::time::sleep(wait).await;
            }
        }
    }
}
